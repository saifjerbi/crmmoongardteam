/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGard.GestionContactMBeans;

import com.MoonGardCRM.GestionContactCRUDSessions.ClientFacadeLocal;
import com.MoonGardCRM.GestionContactEntities.Client;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Jerbi
 */
@ManagedBean
@SessionScoped
public class clientMBean {
    @EJB
    private ClientFacadeLocal clientFacade;
    
    private Client client;

    /**
     * Creates a new instance of clientMBean
     */
    public clientMBean() {
    }
    
    /**
     * Renvoie la liste des clients pour affichage dans une DataTable
     * @return 
     */
    public List<Client> getAllClients() {
        return clientFacade.findAll();
    }
    
    /**
     * Renvoie les détails du client courant (celui dans l'attribut customer de
     * cette classe), qu'on appelle une propriété (property)
     *
     * @return
     */
    public Client getDetails() {
        return client;
    }
    
    /**
     * Action handler - appelé lorsque l'utilisateur sélectionne une ligne dans
     * la DataTable pour voir les détails
     *
     * @param client
     * @return
     */
    public String showDetails(Client client) {
        this.client = client;
        return "Client Details";
    }

    /**
     * Action handler - met à jour la base de données en fonction du client
     * passé en paramètres
     *
     * @return
     */
    public Client update(Client client) {
        System.out.println("###UPDATE###");
        return clientFacade.edit(client);
    }

    /**
     * Action handler - renvoie vers la page qui affiche la liste des clients
     *
     * @return
     */
    public String list() {
        System.out.println("###LIST###");
        return "CustomerList";
    }
}
