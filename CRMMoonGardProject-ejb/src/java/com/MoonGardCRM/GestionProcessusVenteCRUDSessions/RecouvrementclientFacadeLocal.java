/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionProcessusVenteCRUDSessions;

import com.MoonGardCRM.GestionProcessusVenteEntities.Recouvrementclient;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Jerbi
 */
@Local
public interface RecouvrementclientFacadeLocal {

    void create(Recouvrementclient recouvrementclient);

    Recouvrementclient edit(Recouvrementclient recouvrementclient);

    void remove(Recouvrementclient recouvrementclient);

    Recouvrementclient find(Object id);

    List<Recouvrementclient> findAll();

    List<Recouvrementclient> findRange(int[] range);

    int count();
    
}
