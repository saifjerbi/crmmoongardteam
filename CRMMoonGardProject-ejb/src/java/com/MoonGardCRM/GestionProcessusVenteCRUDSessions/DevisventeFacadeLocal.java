/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionProcessusVenteCRUDSessions;

import com.MoonGardCRM.GestionProcessusVenteEntities.Devisvente;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Jerbi
 */
@Local
public interface DevisventeFacadeLocal {

    void create(Devisvente devisvente);

    Devisvente edit(Devisvente devisvente);

    void remove(Devisvente devisvente);

    Devisvente find(Object id);

    List<Devisvente> findAll();

    List<Devisvente> findRange(int[] range);

    int count();
    
}
