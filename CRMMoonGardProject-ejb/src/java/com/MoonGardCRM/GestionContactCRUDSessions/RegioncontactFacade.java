/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionContactCRUDSessions;

import com.MoonGardCRM.CRUDAbstractFacade.AbstractFacade;
import com.MoonGardCRM.GestionContactEntities.Regioncontact;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Jerbi
 */
@Stateless
public class RegioncontactFacade extends AbstractFacade<Regioncontact> implements RegioncontactFacadeLocal {
    @PersistenceContext(unitName = "CRMMoonGardProject-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public RegioncontactFacade() {
        super(Regioncontact.class);
    }
    
}
