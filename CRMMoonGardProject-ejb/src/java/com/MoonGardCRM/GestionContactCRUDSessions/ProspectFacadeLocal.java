/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionContactCRUDSessions;

import com.MoonGardCRM.GestionContactEntities.Prospect;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Jerbi
 */
@Local
public interface ProspectFacadeLocal {

    void create(Prospect prospect);

    Prospect edit(Prospect prospect);

    void remove(Prospect prospect);

    Prospect find(Object id);

    List<Prospect> findAll();

    List<Prospect> findRange(int[] range);

    int count();
    
}
