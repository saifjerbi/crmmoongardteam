/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionMarketingEntities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Jerbi
 */
@Embeddable
public class NewslettercontactPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "refnewsletter")
    private long refnewsletter;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idcontact")
    private long idcontact;

    public NewslettercontactPK() {
    }

    public NewslettercontactPK(long refnewsletter, long idcontact) {
        this.refnewsletter = refnewsletter;
        this.idcontact = idcontact;
    }

    public long getRefnewsletter() {
        return refnewsletter;
    }

    public void setRefnewsletter(long refnewsletter) {
        this.refnewsletter = refnewsletter;
    }

    public long getIdcontact() {
        return idcontact;
    }

    public void setIdcontact(long idcontact) {
        this.idcontact = idcontact;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) refnewsletter;
        hash += (int) idcontact;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NewslettercontactPK)) {
            return false;
        }
        NewslettercontactPK other = (NewslettercontactPK) object;
        if (this.refnewsletter != other.refnewsletter) {
            return false;
        }
        if (this.idcontact != other.idcontact) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.MoonGardCRM.GestionMarketingEntities.NewslettercontactPK[ refnewsletter=" + refnewsletter + ", idcontact=" + idcontact + " ]";
    }
    
}
