/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionRessourceEntities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jerbi
 */
@Entity
@Table(name = "rechangepiece")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Rechangepiece.findAll", query = "SELECT r FROM Rechangepiece r"),
    @NamedQuery(name = "Rechangepiece.findByRefentretienvehicule", query = "SELECT r FROM Rechangepiece r WHERE r.refentretienvehicule = :refentretienvehicule"),
    @NamedQuery(name = "Rechangepiece.findByNommecanicien", query = "SELECT r FROM Rechangepiece r WHERE r.nommecanicien = :nommecanicien"),
    @NamedQuery(name = "Rechangepiece.findByCausedegat", query = "SELECT r FROM Rechangepiece r WHERE r.causedegat = :causedegat"),
    @NamedQuery(name = "Rechangepiece.findByLieu", query = "SELECT r FROM Rechangepiece r WHERE r.lieu = :lieu")})
public class Rechangepiece implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "refentretienvehicule")
    private Long refentretienvehicule;
    @Size(max = 254)
    @Column(name = "nommecanicien")
    private String nommecanicien;
    @Size(max = 254)
    @Column(name = "causedegat")
    private String causedegat;
    @Size(max = 254)
    @Column(name = "lieu")
    private String lieu;
    @OneToMany(mappedBy = "refentretienvehicule")
    private Collection<Piecederechange> piecederechangeCollection;
    @JoinColumn(name = "refentretienvehicule", referencedColumnName = "refentretienvehicule", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Entretienvehicule entretienvehicule;

    public Rechangepiece() {
    }

    public Rechangepiece(Long refentretienvehicule) {
        this.refentretienvehicule = refentretienvehicule;
    }

    public Long getRefentretienvehicule() {
        return refentretienvehicule;
    }

    public void setRefentretienvehicule(Long refentretienvehicule) {
        this.refentretienvehicule = refentretienvehicule;
    }

    public String getNommecanicien() {
        return nommecanicien;
    }

    public void setNommecanicien(String nommecanicien) {
        this.nommecanicien = nommecanicien;
    }

    public String getCausedegat() {
        return causedegat;
    }

    public void setCausedegat(String causedegat) {
        this.causedegat = causedegat;
    }

    public String getLieu() {
        return lieu;
    }

    public void setLieu(String lieu) {
        this.lieu = lieu;
    }

    @XmlTransient
    public Collection<Piecederechange> getPiecederechangeCollection() {
        return piecederechangeCollection;
    }

    public void setPiecederechangeCollection(Collection<Piecederechange> piecederechangeCollection) {
        this.piecederechangeCollection = piecederechangeCollection;
    }

    public Entretienvehicule getEntretienvehicule() {
        return entretienvehicule;
    }

    public void setEntretienvehicule(Entretienvehicule entretienvehicule) {
        this.entretienvehicule = entretienvehicule;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refentretienvehicule != null ? refentretienvehicule.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Rechangepiece)) {
            return false;
        }
        Rechangepiece other = (Rechangepiece) object;
        if ((this.refentretienvehicule == null && other.refentretienvehicule != null) || (this.refentretienvehicule != null && !this.refentretienvehicule.equals(other.refentretienvehicule))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.MoonGardCRM.GestionRessourceEntities.Rechangepiece[ refentretienvehicule=" + refentretienvehicule + " ]";
    }
    
}
