/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionProduitCRUDSessions;

import com.MoonGardCRM.GestionProduitEntities.Tarifproduit;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Jerbi
 */
@Local
public interface TarifproduitFacadeLocal {

    void create(Tarifproduit tarifproduit);

    Tarifproduit edit(Tarifproduit tarifproduit);

    void remove(Tarifproduit tarifproduit);

    Tarifproduit find(Object id);

    List<Tarifproduit> findAll();

    List<Tarifproduit> findRange(int[] range);

    int count();
    
}
