/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionProcessusVenteEntities;

import com.MoonGardCRM.GestionRessourceEntities.Entretienvehicule;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jerbi
 */
@Entity
@Table(name = "vehiculelivraison")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Vehiculelivraison.findAll", query = "SELECT v FROM Vehiculelivraison v"),
    @NamedQuery(name = "Vehiculelivraison.findByRefvehiculelivraison", query = "SELECT v FROM Vehiculelivraison v WHERE v.refvehiculelivraison = :refvehiculelivraison"),
    @NamedQuery(name = "Vehiculelivraison.findByImmatriculation", query = "SELECT v FROM Vehiculelivraison v WHERE v.immatriculation = :immatriculation"),
    @NamedQuery(name = "Vehiculelivraison.findByMarque", query = "SELECT v FROM Vehiculelivraison v WHERE v.marque = :marque"),
    @NamedQuery(name = "Vehiculelivraison.findByModele", query = "SELECT v FROM Vehiculelivraison v WHERE v.modele = :modele"),
    @NamedQuery(name = "Vehiculelivraison.findByCouleur", query = "SELECT v FROM Vehiculelivraison v WHERE v.couleur = :couleur"),
    @NamedQuery(name = "Vehiculelivraison.findByCarburant", query = "SELECT v FROM Vehiculelivraison v WHERE v.carburant = :carburant"),
    @NamedQuery(name = "Vehiculelivraison.findByPoidsmax", query = "SELECT v FROM Vehiculelivraison v WHERE v.poidsmax = :poidsmax"),
    @NamedQuery(name = "Vehiculelivraison.findByDerniervidange", query = "SELECT v FROM Vehiculelivraison v WHERE v.derniervidange = :derniervidange"),
    @NamedQuery(name = "Vehiculelivraison.findByKilometrage", query = "SELECT v FROM Vehiculelivraison v WHERE v.kilometrage = :kilometrage"),
    @NamedQuery(name = "Vehiculelivraison.findByDateachat", query = "SELECT v FROM Vehiculelivraison v WHERE v.dateachat = :dateachat"),
    @NamedQuery(name = "Vehiculelivraison.findByPrixachat", query = "SELECT v FROM Vehiculelivraison v WHERE v.prixachat = :prixachat"),
    @NamedQuery(name = "Vehiculelivraison.findByDescription", query = "SELECT v FROM Vehiculelivraison v WHERE v.description = :description")})
public class Vehiculelivraison implements Serializable {
    @OneToMany(mappedBy = "refvehiculelivraison")
    private Collection<Entretienvehicule> entretienvehiculeCollection;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "refvehiculelivraison")
    private Long refvehiculelivraison;
    @Size(max = 254)
    @Column(name = "immatriculation")
    private String immatriculation;
    @Size(max = 254)
    @Column(name = "marque")
    private String marque;
    @Size(max = 254)
    @Column(name = "modele")
    private String modele;
    @Size(max = 254)
    @Column(name = "couleur")
    private String couleur;
    @Size(max = 254)
    @Column(name = "carburant")
    private String carburant;
    @Column(name = "poidsmax")
    private Integer poidsmax;
    @Column(name = "derniervidange")
    @Temporal(TemporalType.DATE)
    private Date derniervidange;
    @Column(name = "kilometrage")
    private BigInteger kilometrage;
    @Column(name = "dateachat")
    @Temporal(TemporalType.DATE)
    private Date dateachat;
    @Column(name = "prixachat")
    private BigInteger prixachat;
    @Size(max = 254)
    @Column(name = "description")
    private String description;
    @OneToMany(mappedBy = "refvehiculelivraison")
    private Collection<Livraisonvente> livraisonventeCollection;
    @JoinColumn(name = "refchaffeure", referencedColumnName = "refchaffeure")
    @ManyToOne
    private Chauffeur refchaffeure;

    public Vehiculelivraison() {
    }

    public Vehiculelivraison(Long refvehiculelivraison) {
        this.refvehiculelivraison = refvehiculelivraison;
    }

    public Long getRefvehiculelivraison() {
        return refvehiculelivraison;
    }

    public void setRefvehiculelivraison(Long refvehiculelivraison) {
        this.refvehiculelivraison = refvehiculelivraison;
    }

    public String getImmatriculation() {
        return immatriculation;
    }

    public void setImmatriculation(String immatriculation) {
        this.immatriculation = immatriculation;
    }

    public String getMarque() {
        return marque;
    }

    public void setMarque(String marque) {
        this.marque = marque;
    }

    public String getModele() {
        return modele;
    }

    public void setModele(String modele) {
        this.modele = modele;
    }

    public String getCouleur() {
        return couleur;
    }

    public void setCouleur(String couleur) {
        this.couleur = couleur;
    }

    public String getCarburant() {
        return carburant;
    }

    public void setCarburant(String carburant) {
        this.carburant = carburant;
    }

    public Integer getPoidsmax() {
        return poidsmax;
    }

    public void setPoidsmax(Integer poidsmax) {
        this.poidsmax = poidsmax;
    }

    public Date getDerniervidange() {
        return derniervidange;
    }

    public void setDerniervidange(Date derniervidange) {
        this.derniervidange = derniervidange;
    }

    public BigInteger getKilometrage() {
        return kilometrage;
    }

    public void setKilometrage(BigInteger kilometrage) {
        this.kilometrage = kilometrage;
    }

    public Date getDateachat() {
        return dateachat;
    }

    public void setDateachat(Date dateachat) {
        this.dateachat = dateachat;
    }

    public BigInteger getPrixachat() {
        return prixachat;
    }

    public void setPrixachat(BigInteger prixachat) {
        this.prixachat = prixachat;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @XmlTransient
    public Collection<Livraisonvente> getLivraisonventeCollection() {
        return livraisonventeCollection;
    }

    public void setLivraisonventeCollection(Collection<Livraisonvente> livraisonventeCollection) {
        this.livraisonventeCollection = livraisonventeCollection;
    }

    public Chauffeur getRefchaffeure() {
        return refchaffeure;
    }

    public void setRefchaffeure(Chauffeur refchaffeure) {
        this.refchaffeure = refchaffeure;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refvehiculelivraison != null ? refvehiculelivraison.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Vehiculelivraison)) {
            return false;
        }
        Vehiculelivraison other = (Vehiculelivraison) object;
        if ((this.refvehiculelivraison == null && other.refvehiculelivraison != null) || (this.refvehiculelivraison != null && !this.refvehiculelivraison.equals(other.refvehiculelivraison))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.MoonGardCRM.GestionProcessusVenteEntities.Vehiculelivraison[ refvehiculelivraison=" + refvehiculelivraison + " ]";
    }

    @XmlTransient
    public Collection<Entretienvehicule> getEntretienvehiculeCollection() {
        return entretienvehiculeCollection;
    }

    public void setEntretienvehiculeCollection(Collection<Entretienvehicule> entretienvehiculeCollection) {
        this.entretienvehiculeCollection = entretienvehiculeCollection;
    }
    
}
