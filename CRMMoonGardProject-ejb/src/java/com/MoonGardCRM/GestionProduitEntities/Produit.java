/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionProduitEntities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jerbi
 */
@Entity
@Table(name = "produit")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Produit.findAll", query = "SELECT p FROM Produit p"),
    @NamedQuery(name = "Produit.findByReferenceproduit", query = "SELECT p FROM Produit p WHERE p.referenceproduit = :referenceproduit"),
    @NamedQuery(name = "Produit.findByCodeabar", query = "SELECT p FROM Produit p WHERE p.codeabar = :codeabar"),
    @NamedQuery(name = "Produit.findByNomproduit", query = "SELECT p FROM Produit p WHERE p.nomproduit = :nomproduit"),
    @NamedQuery(name = "Produit.findByDesignation", query = "SELECT p FROM Produit p WHERE p.designation = :designation"),
    @NamedQuery(name = "Produit.findByUnitevente", query = "SELECT p FROM Produit p WHERE p.unitevente = :unitevente"),
    @NamedQuery(name = "Produit.findByDatecreation", query = "SELECT p FROM Produit p WHERE p.datecreation = :datecreation"),
    @NamedQuery(name = "Produit.findByDatemodification", query = "SELECT p FROM Produit p WHERE p.datemodification = :datemodification"),
    @NamedQuery(name = "Produit.findByDatelimitevente", query = "SELECT p FROM Produit p WHERE p.datelimitevente = :datelimitevente")})
public class Produit implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "referenceproduit")
    private Long referenceproduit;
    @Size(max = 254)
    @Column(name = "codeabar")
    private String codeabar;
    @Size(max = 254)
    @Column(name = "nomproduit")
    private String nomproduit;
    @Size(max = 254)
    @Column(name = "designation")
    private String designation;
    @Size(max = 254)
    @Column(name = "unitevente")
    private String unitevente;
    @Column(name = "datecreation")
    @Temporal(TemporalType.DATE)
    private Date datecreation;
    @Column(name = "datemodification")
    @Temporal(TemporalType.DATE)
    private Date datemodification;
    @Column(name = "datelimitevente")
    @Temporal(TemporalType.DATE)
    private Date datelimitevente;
    @JoinColumn(name = "reftarifproduit", referencedColumnName = "reftarifproduit")
    @ManyToOne
    private Tarifproduit reftarifproduit;
    @JoinColumn(name = "refstockproduit", referencedColumnName = "refstockproduit")
    @ManyToOne
    private Stockproduit refstockproduit;
    @JoinColumn(name = "reffamilleproduit", referencedColumnName = "reffamilleproduit")
    @ManyToOne
    private Familleproduit reffamilleproduit;
    @JoinColumn(name = "refcategorieproduit", referencedColumnName = "refcategorieproduit")
    @ManyToOne
    private Categorieproduit refcategorieproduit;
    @JoinColumn(name = "refcatalogueproduit", referencedColumnName = "refcatalogueproduit")
    @ManyToOne
    private Catalogueproduit refcatalogueproduit;

    public Produit() {
    }

    public Produit(Long referenceproduit) {
        this.referenceproduit = referenceproduit;
    }

    public Long getReferenceproduit() {
        return referenceproduit;
    }

    public void setReferenceproduit(Long referenceproduit) {
        this.referenceproduit = referenceproduit;
    }

    public String getCodeabar() {
        return codeabar;
    }

    public void setCodeabar(String codeabar) {
        this.codeabar = codeabar;
    }

    public String getNomproduit() {
        return nomproduit;
    }

    public void setNomproduit(String nomproduit) {
        this.nomproduit = nomproduit;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getUnitevente() {
        return unitevente;
    }

    public void setUnitevente(String unitevente) {
        this.unitevente = unitevente;
    }

    public Date getDatecreation() {
        return datecreation;
    }

    public void setDatecreation(Date datecreation) {
        this.datecreation = datecreation;
    }

    public Date getDatemodification() {
        return datemodification;
    }

    public void setDatemodification(Date datemodification) {
        this.datemodification = datemodification;
    }

    public Date getDatelimitevente() {
        return datelimitevente;
    }

    public void setDatelimitevente(Date datelimitevente) {
        this.datelimitevente = datelimitevente;
    }

    public Tarifproduit getReftarifproduit() {
        return reftarifproduit;
    }

    public void setReftarifproduit(Tarifproduit reftarifproduit) {
        this.reftarifproduit = reftarifproduit;
    }

    public Stockproduit getRefstockproduit() {
        return refstockproduit;
    }

    public void setRefstockproduit(Stockproduit refstockproduit) {
        this.refstockproduit = refstockproduit;
    }

    public Familleproduit getReffamilleproduit() {
        return reffamilleproduit;
    }

    public void setReffamilleproduit(Familleproduit reffamilleproduit) {
        this.reffamilleproduit = reffamilleproduit;
    }

    public Categorieproduit getRefcategorieproduit() {
        return refcategorieproduit;
    }

    public void setRefcategorieproduit(Categorieproduit refcategorieproduit) {
        this.refcategorieproduit = refcategorieproduit;
    }

    public Catalogueproduit getRefcatalogueproduit() {
        return refcatalogueproduit;
    }

    public void setRefcatalogueproduit(Catalogueproduit refcatalogueproduit) {
        this.refcatalogueproduit = refcatalogueproduit;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (referenceproduit != null ? referenceproduit.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Produit)) {
            return false;
        }
        Produit other = (Produit) object;
        if ((this.referenceproduit == null && other.referenceproduit != null) || (this.referenceproduit != null && !this.referenceproduit.equals(other.referenceproduit))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.MoonGardCRM.GestionProduitEntities.Produit[ referenceproduit=" + referenceproduit + " ]";
    }
    
}
