/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionContactCRUDSessions;

import com.MoonGardCRM.GestionContactEntities.Contactarchivee;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Jerbi
 */
@Local
public interface ContactarchiveeFacadeLocal {

    void create(Contactarchivee contactarchivee);

    Contactarchivee edit(Contactarchivee contactarchivee);

    void remove(Contactarchivee contactarchivee);

    Contactarchivee find(Object id);

    List<Contactarchivee> findAll();

    List<Contactarchivee> findRange(int[] range);

    int count();
    
}
