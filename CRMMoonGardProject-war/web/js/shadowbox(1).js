Shadowbox.init({
    language:   "fr",
    players:    ['img','iframe'],
    autoDimensions: true
});	
function Shado(url, largeur, hauteur){
    Shadowbox.open({
        content:    url,
        player:     "iframe",
        width: largeur,
        height: hauteur
    });
};