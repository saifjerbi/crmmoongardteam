/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionContactEntities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jerbi
 */
@Entity
@Table(name = "pays")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Pays.findAll", query = "SELECT p FROM Pays p"),
    @NamedQuery(name = "Pays.findByRefpays", query = "SELECT p FROM Pays p WHERE p.refpays = :refpays"),
    @NamedQuery(name = "Pays.findByNom", query = "SELECT p FROM Pays p WHERE p.nom = :nom"),
    @NamedQuery(name = "Pays.findByCallprefix", query = "SELECT p FROM Pays p WHERE p.callprefix = :callprefix")})
public class Pays implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "refpays")
    private Integer refpays;
    @Size(max = 254)
    @Column(name = "nom")
    private String nom;
    @Size(max = 254)
    @Column(name = "callprefix")
    private String callprefix;

    public Pays() {
    }

    public Pays(Integer refpays) {
        this.refpays = refpays;
    }

    public Integer getRefpays() {
        return refpays;
    }

    public void setRefpays(Integer refpays) {
        this.refpays = refpays;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getCallprefix() {
        return callprefix;
    }

    public void setCallprefix(String callprefix) {
        this.callprefix = callprefix;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refpays != null ? refpays.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pays)) {
            return false;
        }
        Pays other = (Pays) object;
        if ((this.refpays == null && other.refpays != null) || (this.refpays != null && !this.refpays.equals(other.refpays))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.MoonGardCRM.GestionContactEntities.Pays[ refpays=" + refpays + " ]";
    }
    
}
