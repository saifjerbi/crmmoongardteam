/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionProcessusVenteEntities;

import com.MoonGardCRM.GestionProduitEntities.Produit;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jerbi
 */
@Entity
@Table(name = "entreestockfactureavoirclient")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Entreestockfactureavoirclient.findAll", query = "SELECT e FROM Entreestockfactureavoirclient e"),
    @NamedQuery(name = "Entreestockfactureavoirclient.findByNumerooperation", query = "SELECT e FROM Entreestockfactureavoirclient e WHERE e.numerooperation = :numerooperation"),
    @NamedQuery(name = "Entreestockfactureavoirclient.findByQuantite", query = "SELECT e FROM Entreestockfactureavoirclient e WHERE e.quantite = :quantite"),
    @NamedQuery(name = "Entreestockfactureavoirclient.findByDateoperation", query = "SELECT e FROM Entreestockfactureavoirclient e WHERE e.dateoperation = :dateoperation"),
    @NamedQuery(name = "Entreestockfactureavoirclient.findByHeureoperation", query = "SELECT e FROM Entreestockfactureavoirclient e WHERE e.heureoperation = :heureoperation")})
public class Entreestockfactureavoirclient implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "numerooperation")
    private Long numerooperation;
    @Column(name = "quantite")
    private BigInteger quantite;
    @Column(name = "dateoperation")
    @Temporal(TemporalType.DATE)
    private Date dateoperation;
    @Size(max = 254)
    @Column(name = "heureoperation")
    private String heureoperation;
    @JoinColumn(name = "referenceproduit", referencedColumnName = "referenceproduit")
    @ManyToOne
    private Produit referenceproduit;
    @JoinColumn(name = "idligne", referencedColumnName = "idligne")
    @ManyToOne
    private Lignefactureavoirvente idligne;

    public Entreestockfactureavoirclient() {
    }

    public Entreestockfactureavoirclient(Long numerooperation) {
        this.numerooperation = numerooperation;
    }

    public Long getNumerooperation() {
        return numerooperation;
    }

    public void setNumerooperation(Long numerooperation) {
        this.numerooperation = numerooperation;
    }

    public BigInteger getQuantite() {
        return quantite;
    }

    public void setQuantite(BigInteger quantite) {
        this.quantite = quantite;
    }

    public Date getDateoperation() {
        return dateoperation;
    }

    public void setDateoperation(Date dateoperation) {
        this.dateoperation = dateoperation;
    }

    public String getHeureoperation() {
        return heureoperation;
    }

    public void setHeureoperation(String heureoperation) {
        this.heureoperation = heureoperation;
    }

    public Produit getReferenceproduit() {
        return referenceproduit;
    }

    public void setReferenceproduit(Produit referenceproduit) {
        this.referenceproduit = referenceproduit;
    }

    public Lignefactureavoirvente getIdligne() {
        return idligne;
    }

    public void setIdligne(Lignefactureavoirvente idligne) {
        this.idligne = idligne;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (numerooperation != null ? numerooperation.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Entreestockfactureavoirclient)) {
            return false;
        }
        Entreestockfactureavoirclient other = (Entreestockfactureavoirclient) object;
        if ((this.numerooperation == null && other.numerooperation != null) || (this.numerooperation != null && !this.numerooperation.equals(other.numerooperation))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.MoonGardCRM.GestionProcessusVenteEntities.Entreestockfactureavoirclient[ numerooperation=" + numerooperation + " ]";
    }
    
}
