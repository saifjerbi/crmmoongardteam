/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionRessourceEntities;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jerbi
 */
@Entity
@Table(name = "piecederechange")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Piecederechange.findAll", query = "SELECT p FROM Piecederechange p"),
    @NamedQuery(name = "Piecederechange.findByRefpiecederechange", query = "SELECT p FROM Piecederechange p WHERE p.refpiecederechange = :refpiecederechange"),
    @NamedQuery(name = "Piecederechange.findByNompiece", query = "SELECT p FROM Piecederechange p WHERE p.nompiece = :nompiece"),
    @NamedQuery(name = "Piecederechange.findByMarque", query = "SELECT p FROM Piecederechange p WHERE p.marque = :marque"),
    @NamedQuery(name = "Piecederechange.findByPrixachat", query = "SELECT p FROM Piecederechange p WHERE p.prixachat = :prixachat")})
public class Piecederechange implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "refpiecederechange")
    private Long refpiecederechange;
    @Size(max = 254)
    @Column(name = "nompiece")
    private String nompiece;
    @Size(max = 254)
    @Column(name = "marque")
    private String marque;
    @Column(name = "prixachat")
    private BigInteger prixachat;
    @JoinColumn(name = "refentretienvehicule", referencedColumnName = "refentretienvehicule")
    @ManyToOne
    private Rechangepiece refentretienvehicule;

    public Piecederechange() {
    }

    public Piecederechange(Long refpiecederechange) {
        this.refpiecederechange = refpiecederechange;
    }

    public Long getRefpiecederechange() {
        return refpiecederechange;
    }

    public void setRefpiecederechange(Long refpiecederechange) {
        this.refpiecederechange = refpiecederechange;
    }

    public String getNompiece() {
        return nompiece;
    }

    public void setNompiece(String nompiece) {
        this.nompiece = nompiece;
    }

    public String getMarque() {
        return marque;
    }

    public void setMarque(String marque) {
        this.marque = marque;
    }

    public BigInteger getPrixachat() {
        return prixachat;
    }

    public void setPrixachat(BigInteger prixachat) {
        this.prixachat = prixachat;
    }

    public Rechangepiece getRefentretienvehicule() {
        return refentretienvehicule;
    }

    public void setRefentretienvehicule(Rechangepiece refentretienvehicule) {
        this.refentretienvehicule = refentretienvehicule;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refpiecederechange != null ? refpiecederechange.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Piecederechange)) {
            return false;
        }
        Piecederechange other = (Piecederechange) object;
        if ((this.refpiecederechange == null && other.refpiecederechange != null) || (this.refpiecederechange != null && !this.refpiecederechange.equals(other.refpiecederechange))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.MoonGardCRM.GestionRessourceEntities.Piecederechange[ refpiecederechange=" + refpiecederechange + " ]";
    }
    
}
