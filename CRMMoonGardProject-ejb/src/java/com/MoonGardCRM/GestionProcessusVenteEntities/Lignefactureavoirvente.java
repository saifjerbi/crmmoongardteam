/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionProcessusVenteEntities;

import com.MoonGardCRM.GestionProduitEntities.Produit;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jerbi
 */
@Entity
@Table(name = "lignefactureavoirvente")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Lignefactureavoirvente.findAll", query = "SELECT l FROM Lignefactureavoirvente l"),
    @NamedQuery(name = "Lignefactureavoirvente.findByIdligne", query = "SELECT l FROM Lignefactureavoirvente l WHERE l.idligne = :idligne"),
    @NamedQuery(name = "Lignefactureavoirvente.findByQuantite", query = "SELECT l FROM Lignefactureavoirvente l WHERE l.quantite = :quantite"),
    @NamedQuery(name = "Lignefactureavoirvente.findByUnite", query = "SELECT l FROM Lignefactureavoirvente l WHERE l.unite = :unite"),
    @NamedQuery(name = "Lignefactureavoirvente.findByMontanttotalht", query = "SELECT l FROM Lignefactureavoirvente l WHERE l.montanttotalht = :montanttotalht"),
    @NamedQuery(name = "Lignefactureavoirvente.findByMontanttotalttc", query = "SELECT l FROM Lignefactureavoirvente l WHERE l.montanttotalttc = :montanttotalttc"),
    @NamedQuery(name = "Lignefactureavoirvente.findByTotaltva", query = "SELECT l FROM Lignefactureavoirvente l WHERE l.totaltva = :totaltva"),
    @NamedQuery(name = "Lignefactureavoirvente.findByRemise", query = "SELECT l FROM Lignefactureavoirvente l WHERE l.remise = :remise")})
public class Lignefactureavoirvente implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "idligne")
    private Integer idligne;
    @Column(name = "quantite")
    private BigInteger quantite;
    @Size(max = 254)
    @Column(name = "unite")
    private String unite;
    @Column(name = "montanttotalht")
    private BigInteger montanttotalht;
    @Column(name = "montanttotalttc")
    private BigInteger montanttotalttc;
    @Column(name = "totaltva")
    private BigInteger totaltva;
    @Column(name = "remise")
    private BigInteger remise;
    @OneToMany(mappedBy = "idligne")
    private Collection<Entreestockfactureavoirclient> entreestockfactureavoirclientCollection;
    @JoinColumn(name = "referenceproduit", referencedColumnName = "referenceproduit")
    @ManyToOne
    private Produit referenceproduit;
    @JoinColumn(name = "numerofact", referencedColumnName = "numerofact")
    @ManyToOne
    private Factureavoirvente numerofact;

    public Lignefactureavoirvente() {
    }

    public Lignefactureavoirvente(Integer idligne) {
        this.idligne = idligne;
    }

    public Integer getIdligne() {
        return idligne;
    }

    public void setIdligne(Integer idligne) {
        this.idligne = idligne;
    }

    public BigInteger getQuantite() {
        return quantite;
    }

    public void setQuantite(BigInteger quantite) {
        this.quantite = quantite;
    }

    public String getUnite() {
        return unite;
    }

    public void setUnite(String unite) {
        this.unite = unite;
    }

    public BigInteger getMontanttotalht() {
        return montanttotalht;
    }

    public void setMontanttotalht(BigInteger montanttotalht) {
        this.montanttotalht = montanttotalht;
    }

    public BigInteger getMontanttotalttc() {
        return montanttotalttc;
    }

    public void setMontanttotalttc(BigInteger montanttotalttc) {
        this.montanttotalttc = montanttotalttc;
    }

    public BigInteger getTotaltva() {
        return totaltva;
    }

    public void setTotaltva(BigInteger totaltva) {
        this.totaltva = totaltva;
    }

    public BigInteger getRemise() {
        return remise;
    }

    public void setRemise(BigInteger remise) {
        this.remise = remise;
    }

    @XmlTransient
    public Collection<Entreestockfactureavoirclient> getEntreestockfactureavoirclientCollection() {
        return entreestockfactureavoirclientCollection;
    }

    public void setEntreestockfactureavoirclientCollection(Collection<Entreestockfactureavoirclient> entreestockfactureavoirclientCollection) {
        this.entreestockfactureavoirclientCollection = entreestockfactureavoirclientCollection;
    }

    public Produit getReferenceproduit() {
        return referenceproduit;
    }

    public void setReferenceproduit(Produit referenceproduit) {
        this.referenceproduit = referenceproduit;
    }

    public Factureavoirvente getNumerofact() {
        return numerofact;
    }

    public void setNumerofact(Factureavoirvente numerofact) {
        this.numerofact = numerofact;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idligne != null ? idligne.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Lignefactureavoirvente)) {
            return false;
        }
        Lignefactureavoirvente other = (Lignefactureavoirvente) object;
        if ((this.idligne == null && other.idligne != null) || (this.idligne != null && !this.idligne.equals(other.idligne))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.MoonGardCRM.GestionProcessusVenteEntities.Lignefactureavoirvente[ idligne=" + idligne + " ]";
    }
    
}
