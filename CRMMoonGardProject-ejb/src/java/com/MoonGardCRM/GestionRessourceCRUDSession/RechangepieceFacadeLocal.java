/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionRessourceCRUDSession;

import com.MoonGardCRM.GestionRessourceEntities.Rechangepiece;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Jerbi
 */
@Local
public interface RechangepieceFacadeLocal {

    void create(Rechangepiece rechangepiece);

    Rechangepiece edit(Rechangepiece rechangepiece);

    void remove(Rechangepiece rechangepiece);

    Rechangepiece find(Object id);

    List<Rechangepiece> findAll();

    List<Rechangepiece> findRange(int[] range);

    int count();
    
}
