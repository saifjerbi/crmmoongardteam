/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionProcessusVenteEntities;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jerbi
 */
@Entity
@Table(name = "factureavoirvente")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Factureavoirvente.findAll", query = "SELECT f FROM Factureavoirvente f"),
    @NamedQuery(name = "Factureavoirvente.findByNumerofact", query = "SELECT f FROM Factureavoirvente f WHERE f.numerofact = :numerofact"),
    @NamedQuery(name = "Factureavoirvente.findByDatecreation", query = "SELECT f FROM Factureavoirvente f WHERE f.datecreation = :datecreation"),
    @NamedQuery(name = "Factureavoirvente.findByTotalttc", query = "SELECT f FROM Factureavoirvente f WHERE f.totalttc = :totalttc"),
    @NamedQuery(name = "Factureavoirvente.findByTotalht", query = "SELECT f FROM Factureavoirvente f WHERE f.totalht = :totalht"),
    @NamedQuery(name = "Factureavoirvente.findByTotaltva", query = "SELECT f FROM Factureavoirvente f WHERE f.totaltva = :totaltva"),
    @NamedQuery(name = "Factureavoirvente.findByMessageentete", query = "SELECT f FROM Factureavoirvente f WHERE f.messageentete = :messageentete"),
    @NamedQuery(name = "Factureavoirvente.findByNotebaspage", query = "SELECT f FROM Factureavoirvente f WHERE f.notebaspage = :notebaspage"),
    @NamedQuery(name = "Factureavoirvente.findByEtat", query = "SELECT f FROM Factureavoirvente f WHERE f.etat = :etat")})
public class Factureavoirvente implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "numerofact")
    private Long numerofact;
    @Column(name = "datecreation")
    @Temporal(TemporalType.DATE)
    private Date datecreation;
    @Column(name = "totalttc")
    private BigInteger totalttc;
    @Column(name = "totalht")
    private BigInteger totalht;
    @Column(name = "totaltva")
    private BigInteger totaltva;
    @Size(max = 254)
    @Column(name = "messageentete")
    private String messageentete;
    @Size(max = 254)
    @Column(name = "notebaspage")
    private String notebaspage;
    @Size(max = 254)
    @Column(name = "etat")
    private String etat;
    @JoinColumn(name = "numerofact", referencedColumnName = "numerofact", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Facturevente facturevente;
    @OneToMany(mappedBy = "numerofact")
    private Collection<Lignefactureavoirvente> lignefactureavoirventeCollection;

    public Factureavoirvente() {
    }

    public Factureavoirvente(Long numerofact) {
        this.numerofact = numerofact;
    }

    public Long getNumerofact() {
        return numerofact;
    }

    public void setNumerofact(Long numerofact) {
        this.numerofact = numerofact;
    }

    public Date getDatecreation() {
        return datecreation;
    }

    public void setDatecreation(Date datecreation) {
        this.datecreation = datecreation;
    }

    public BigInteger getTotalttc() {
        return totalttc;
    }

    public void setTotalttc(BigInteger totalttc) {
        this.totalttc = totalttc;
    }

    public BigInteger getTotalht() {
        return totalht;
    }

    public void setTotalht(BigInteger totalht) {
        this.totalht = totalht;
    }

    public BigInteger getTotaltva() {
        return totaltva;
    }

    public void setTotaltva(BigInteger totaltva) {
        this.totaltva = totaltva;
    }

    public String getMessageentete() {
        return messageentete;
    }

    public void setMessageentete(String messageentete) {
        this.messageentete = messageentete;
    }

    public String getNotebaspage() {
        return notebaspage;
    }

    public void setNotebaspage(String notebaspage) {
        this.notebaspage = notebaspage;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public Facturevente getFacturevente() {
        return facturevente;
    }

    public void setFacturevente(Facturevente facturevente) {
        this.facturevente = facturevente;
    }

    @XmlTransient
    public Collection<Lignefactureavoirvente> getLignefactureavoirventeCollection() {
        return lignefactureavoirventeCollection;
    }

    public void setLignefactureavoirventeCollection(Collection<Lignefactureavoirvente> lignefactureavoirventeCollection) {
        this.lignefactureavoirventeCollection = lignefactureavoirventeCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (numerofact != null ? numerofact.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Factureavoirvente)) {
            return false;
        }
        Factureavoirvente other = (Factureavoirvente) object;
        if ((this.numerofact == null && other.numerofact != null) || (this.numerofact != null && !this.numerofact.equals(other.numerofact))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.MoonGardCRM.GestionProcessusVenteEntities.Factureavoirvente[ numerofact=" + numerofact + " ]";
    }
    
}
