/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionProcessusVenteEntities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jerbi
 */
@Entity
@Table(name = "payementfacturevente")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Payementfacturevente.findAll", query = "SELECT p FROM Payementfacturevente p"),
    @NamedQuery(name = "Payementfacturevente.findByRefpayementfacturevente", query = "SELECT p FROM Payementfacturevente p WHERE p.refpayementfacturevente = :refpayementfacturevente"),
    @NamedQuery(name = "Payementfacturevente.findByDate", query = "SELECT p FROM Payementfacturevente p WHERE p.date = :date"),
    @NamedQuery(name = "Payementfacturevente.findByType", query = "SELECT p FROM Payementfacturevente p WHERE p.type = :type"),
    @NamedQuery(name = "Payementfacturevente.findByReference", query = "SELECT p FROM Payementfacturevente p WHERE p.reference = :reference"),
    @NamedQuery(name = "Payementfacturevente.findByMontant", query = "SELECT p FROM Payementfacturevente p WHERE p.montant = :montant"),
    @NamedQuery(name = "Payementfacturevente.findByResteapayer", query = "SELECT p FROM Payementfacturevente p WHERE p.resteapayer = :resteapayer")})
public class Payementfacturevente implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "refpayementfacturevente")
    private Long refpayementfacturevente;
    @Column(name = "date")
    private Integer date;
    @Size(max = 254)
    @Column(name = "type")
    private String type;
    @Column(name = "reference")
    private Integer reference;
    @Column(name = "montant")
    private Integer montant;
    @Column(name = "resteapayer")
    private Integer resteapayer;
    @JoinColumn(name = "numerofact", referencedColumnName = "numerofact")
    @ManyToOne
    private Facturevente numerofact;
    @JoinColumn(name = "refecheancier", referencedColumnName = "refecheancier")
    @ManyToOne
    private Echeancier refecheancier;

    public Payementfacturevente() {
    }

    public Payementfacturevente(Long refpayementfacturevente) {
        this.refpayementfacturevente = refpayementfacturevente;
    }

    public Long getRefpayementfacturevente() {
        return refpayementfacturevente;
    }

    public void setRefpayementfacturevente(Long refpayementfacturevente) {
        this.refpayementfacturevente = refpayementfacturevente;
    }

    public Integer getDate() {
        return date;
    }

    public void setDate(Integer date) {
        this.date = date;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getReference() {
        return reference;
    }

    public void setReference(Integer reference) {
        this.reference = reference;
    }

    public Integer getMontant() {
        return montant;
    }

    public void setMontant(Integer montant) {
        this.montant = montant;
    }

    public Integer getResteapayer() {
        return resteapayer;
    }

    public void setResteapayer(Integer resteapayer) {
        this.resteapayer = resteapayer;
    }

    public Facturevente getNumerofact() {
        return numerofact;
    }

    public void setNumerofact(Facturevente numerofact) {
        this.numerofact = numerofact;
    }

    public Echeancier getRefecheancier() {
        return refecheancier;
    }

    public void setRefecheancier(Echeancier refecheancier) {
        this.refecheancier = refecheancier;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refpayementfacturevente != null ? refpayementfacturevente.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Payementfacturevente)) {
            return false;
        }
        Payementfacturevente other = (Payementfacturevente) object;
        if ((this.refpayementfacturevente == null && other.refpayementfacturevente != null) || (this.refpayementfacturevente != null && !this.refpayementfacturevente.equals(other.refpayementfacturevente))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.MoonGardCRM.GestionProcessusVenteEntities.Payementfacturevente[ refpayementfacturevente=" + refpayementfacturevente + " ]";
    }
    
}
