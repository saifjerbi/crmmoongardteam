/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionUtilisateurCRUDSessions;

import com.MoonGardCRM.GestionUtilisateurEntities.Caissier;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Jerbi
 */
@Local
public interface CaissierFacadeLocal {

    void create(Caissier caissier);

    Caissier edit(Caissier caissier);

    void remove(Caissier caissier);

    Caissier find(Object id);

    List<Caissier> findAll();

    List<Caissier> findRange(int[] range);

    int count();
    
}
