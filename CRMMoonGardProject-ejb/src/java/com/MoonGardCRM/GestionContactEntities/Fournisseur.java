/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionContactEntities;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jerbi
 */
@Entity
@Table(name = "fournisseur")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Fournisseur.findAll", query = "SELECT f FROM Fournisseur f"),
    @NamedQuery(name = "Fournisseur.findByIdcontact", query = "SELECT f FROM Fournisseur f WHERE f.idcontact = :idcontact"),
    @NamedQuery(name = "Fournisseur.findByCumuleca", query = "SELECT f FROM Fournisseur f WHERE f.cumuleca = :cumuleca"),
    @NamedQuery(name = "Fournisseur.findByCumulecasolde", query = "SELECT f FROM Fournisseur f WHERE f.cumulecasolde = :cumulecasolde"),
    @NamedQuery(name = "Fournisseur.findByEntreprise", query = "SELECT f FROM Fournisseur f WHERE f.entreprise = :entreprise")})
public class Fournisseur implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "idcontact")
    private Long idcontact;
    @Column(name = "cumuleca")
    private BigInteger cumuleca;
    @Column(name = "cumulecasolde")
    private BigInteger cumulecasolde;
    @Size(max = 254)
    @Column(name = "entreprise")
    private String entreprise;
    @JoinColumn(name = "idcontact", referencedColumnName = "idcontact", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Contact contact;

    public Fournisseur() {
    }

    public Fournisseur(Long idcontact) {
        this.idcontact = idcontact;
    }

    public Long getIdcontact() {
        return idcontact;
    }

    public void setIdcontact(Long idcontact) {
        this.idcontact = idcontact;
    }

    public BigInteger getCumuleca() {
        return cumuleca;
    }

    public void setCumuleca(BigInteger cumuleca) {
        this.cumuleca = cumuleca;
    }

    public BigInteger getCumulecasolde() {
        return cumulecasolde;
    }

    public void setCumulecasolde(BigInteger cumulecasolde) {
        this.cumulecasolde = cumulecasolde;
    }

    public String getEntreprise() {
        return entreprise;
    }

    public void setEntreprise(String entreprise) {
        this.entreprise = entreprise;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idcontact != null ? idcontact.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Fournisseur)) {
            return false;
        }
        Fournisseur other = (Fournisseur) object;
        if ((this.idcontact == null && other.idcontact != null) || (this.idcontact != null && !this.idcontact.equals(other.idcontact))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.MoonGardCRM.GestionContactEntities.Fournisseur[ idcontact=" + idcontact + " ]";
    }
    
}
