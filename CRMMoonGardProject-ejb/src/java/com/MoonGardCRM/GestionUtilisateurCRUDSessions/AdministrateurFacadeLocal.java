/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionUtilisateurCRUDSessions;

import com.MoonGardCRM.GestionUtilisateurEntities.Administrateur;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Jerbi
 */
@Local
public interface AdministrateurFacadeLocal {

    void create(Administrateur administrateur);

    Administrateur edit(Administrateur administrateur);

    void remove(Administrateur administrateur);

    Administrateur find(Object id);

    List<Administrateur> findAll();

    List<Administrateur> findRange(int[] range);

    int count();
    
}
