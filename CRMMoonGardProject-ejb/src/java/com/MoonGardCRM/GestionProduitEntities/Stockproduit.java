/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionProduitEntities;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jerbi
 */
@Entity
@Table(name = "stockproduit")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Stockproduit.findAll", query = "SELECT s FROM Stockproduit s"),
    @NamedQuery(name = "Stockproduit.findByRefstockproduit", query = "SELECT s FROM Stockproduit s WHERE s.refstockproduit = :refstockproduit"),
    @NamedQuery(name = "Stockproduit.findByDatepremierentreestock", query = "SELECT s FROM Stockproduit s WHERE s.datepremierentreestock = :datepremierentreestock"),
    @NamedQuery(name = "Stockproduit.findByDatedernierentreestock", query = "SELECT s FROM Stockproduit s WHERE s.datedernierentreestock = :datedernierentreestock"),
    @NamedQuery(name = "Stockproduit.findByDatepremiersortiestock", query = "SELECT s FROM Stockproduit s WHERE s.datepremiersortiestock = :datepremiersortiestock"),
    @NamedQuery(name = "Stockproduit.findByDatederniersortiestock", query = "SELECT s FROM Stockproduit s WHERE s.datederniersortiestock = :datederniersortiestock"),
    @NamedQuery(name = "Stockproduit.findByQuantiteinitiale", query = "SELECT s FROM Stockproduit s WHERE s.quantiteinitiale = :quantiteinitiale"),
    @NamedQuery(name = "Stockproduit.findByQuantiteencours", query = "SELECT s FROM Stockproduit s WHERE s.quantiteencours = :quantiteencours"),
    @NamedQuery(name = "Stockproduit.findByQuantitemax", query = "SELECT s FROM Stockproduit s WHERE s.quantitemax = :quantitemax"),
    @NamedQuery(name = "Stockproduit.findByQuantitemin", query = "SELECT s FROM Stockproduit s WHERE s.quantitemin = :quantitemin")})
public class Stockproduit implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "refstockproduit")
    private Long refstockproduit;
    @Column(name = "datepremierentreestock")
    @Temporal(TemporalType.DATE)
    private Date datepremierentreestock;
    @Column(name = "datedernierentreestock")
    @Temporal(TemporalType.DATE)
    private Date datedernierentreestock;
    @Column(name = "datepremiersortiestock")
    @Temporal(TemporalType.DATE)
    private Date datepremiersortiestock;
    @Column(name = "datederniersortiestock")
    @Temporal(TemporalType.DATE)
    private Date datederniersortiestock;
    @Column(name = "quantiteinitiale")
    private BigInteger quantiteinitiale;
    @Column(name = "quantiteencours")
    private BigInteger quantiteencours;
    @Column(name = "quantitemax")
    private BigInteger quantitemax;
    @Column(name = "quantitemin")
    private BigInteger quantitemin;
    @OneToMany(mappedBy = "refstockproduit")
    private Collection<Produit> produitCollection;

    public Stockproduit() {
    }

    public Stockproduit(Long refstockproduit) {
        this.refstockproduit = refstockproduit;
    }

    public Long getRefstockproduit() {
        return refstockproduit;
    }

    public void setRefstockproduit(Long refstockproduit) {
        this.refstockproduit = refstockproduit;
    }

    public Date getDatepremierentreestock() {
        return datepremierentreestock;
    }

    public void setDatepremierentreestock(Date datepremierentreestock) {
        this.datepremierentreestock = datepremierentreestock;
    }

    public Date getDatedernierentreestock() {
        return datedernierentreestock;
    }

    public void setDatedernierentreestock(Date datedernierentreestock) {
        this.datedernierentreestock = datedernierentreestock;
    }

    public Date getDatepremiersortiestock() {
        return datepremiersortiestock;
    }

    public void setDatepremiersortiestock(Date datepremiersortiestock) {
        this.datepremiersortiestock = datepremiersortiestock;
    }

    public Date getDatederniersortiestock() {
        return datederniersortiestock;
    }

    public void setDatederniersortiestock(Date datederniersortiestock) {
        this.datederniersortiestock = datederniersortiestock;
    }

    public BigInteger getQuantiteinitiale() {
        return quantiteinitiale;
    }

    public void setQuantiteinitiale(BigInteger quantiteinitiale) {
        this.quantiteinitiale = quantiteinitiale;
    }

    public BigInteger getQuantiteencours() {
        return quantiteencours;
    }

    public void setQuantiteencours(BigInteger quantiteencours) {
        this.quantiteencours = quantiteencours;
    }

    public BigInteger getQuantitemax() {
        return quantitemax;
    }

    public void setQuantitemax(BigInteger quantitemax) {
        this.quantitemax = quantitemax;
    }

    public BigInteger getQuantitemin() {
        return quantitemin;
    }

    public void setQuantitemin(BigInteger quantitemin) {
        this.quantitemin = quantitemin;
    }

    @XmlTransient
    public Collection<Produit> getProduitCollection() {
        return produitCollection;
    }

    public void setProduitCollection(Collection<Produit> produitCollection) {
        this.produitCollection = produitCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refstockproduit != null ? refstockproduit.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Stockproduit)) {
            return false;
        }
        Stockproduit other = (Stockproduit) object;
        if ((this.refstockproduit == null && other.refstockproduit != null) || (this.refstockproduit != null && !this.refstockproduit.equals(other.refstockproduit))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.MoonGardCRM.GestionProduitEntities.Stockproduit[ refstockproduit=" + refstockproduit + " ]";
    }
    
}
