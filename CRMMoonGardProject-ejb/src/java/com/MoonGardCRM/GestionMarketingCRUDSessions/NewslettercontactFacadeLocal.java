/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionMarketingCRUDSessions;

import com.MoonGardCRM.GestionMarketingEntities.Newslettercontact;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Jerbi
 */
@Local
public interface NewslettercontactFacadeLocal {

    void create(Newslettercontact newslettercontact);

    Newslettercontact edit(Newslettercontact newslettercontact);

    void remove(Newslettercontact newslettercontact);

    Newslettercontact find(Object id);

    List<Newslettercontact> findAll();

    List<Newslettercontact> findRange(int[] range);

    int count();
    
}
