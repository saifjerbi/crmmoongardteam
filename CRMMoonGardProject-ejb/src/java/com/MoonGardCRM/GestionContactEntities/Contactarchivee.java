/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionContactEntities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jerbi
 */
@Entity
@Table(name = "contactarchivee")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Contactarchivee.findAll", query = "SELECT c FROM Contactarchivee c"),
    @NamedQuery(name = "Contactarchivee.findByIdcontact", query = "SELECT c FROM Contactarchivee c WHERE c.idcontact = :idcontact"),
    @NamedQuery(name = "Contactarchivee.findByDatearchivage", query = "SELECT c FROM Contactarchivee c WHERE c.datearchivage = :datearchivage")})
public class Contactarchivee implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "idcontact")
    private Long idcontact;
    @Column(name = "datearchivage")
    @Temporal(TemporalType.DATE)
    private Date datearchivage;
    @JoinColumn(name = "idcontact", referencedColumnName = "idcontact", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Contact contact;

    public Contactarchivee() {
    }

    public Contactarchivee(Long idcontact) {
        this.idcontact = idcontact;
    }

    public Long getIdcontact() {
        return idcontact;
    }

    public void setIdcontact(Long idcontact) {
        this.idcontact = idcontact;
    }

    public Date getDatearchivage() {
        return datearchivage;
    }

    public void setDatearchivage(Date datearchivage) {
        this.datearchivage = datearchivage;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idcontact != null ? idcontact.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Contactarchivee)) {
            return false;
        }
        Contactarchivee other = (Contactarchivee) object;
        if ((this.idcontact == null && other.idcontact != null) || (this.idcontact != null && !this.idcontact.equals(other.idcontact))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.MoonGardCRM.GestionContactEntities.Contactarchivee[ idcontact=" + idcontact + " ]";
    }
    
}
