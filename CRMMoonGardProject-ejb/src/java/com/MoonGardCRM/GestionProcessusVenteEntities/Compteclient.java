/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionProcessusVenteEntities;

import com.MoonGardCRM.GestionContactEntities.Client;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jerbi
 */
@Entity
@Table(name = "compteclient")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Compteclient.findAll", query = "SELECT c FROM Compteclient c"),
    @NamedQuery(name = "Compteclient.findByRefcompteclient", query = "SELECT c FROM Compteclient c WHERE c.refcompteclient = :refcompteclient"),
    @NamedQuery(name = "Compteclient.findByPlafondencours", query = "SELECT c FROM Compteclient c WHERE c.plafondencours = :plafondencours"),
    @NamedQuery(name = "Compteclient.findByDatecreation", query = "SELECT c FROM Compteclient c WHERE c.datecreation = :datecreation"),
    @NamedQuery(name = "Compteclient.findByDatedernierupd", query = "SELECT c FROM Compteclient c WHERE c.datedernierupd = :datedernierupd")})
public class Compteclient implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "refcompteclient")
    private Long refcompteclient;
    @Column(name = "plafondencours")
    private BigInteger plafondencours;
    @Column(name = "datecreation")
    @Temporal(TemporalType.DATE)
    private Date datecreation;
    @Column(name = "datedernierupd")
    @Temporal(TemporalType.DATE)
    private Date datedernierupd;
    @JoinColumn(name = "idcontact", referencedColumnName = "idcontact")
    @ManyToOne
    private Client idcontact;
    @OneToMany(mappedBy = "refcompteclient")
    private Collection<Facturevente> factureventeCollection;
    @OneToMany(mappedBy = "refcompteclient")
    private Collection<Recouvrementclient> recouvrementclientCollection;

    public Compteclient() {
    }

    public Compteclient(Long refcompteclient) {
        this.refcompteclient = refcompteclient;
    }

    public Long getRefcompteclient() {
        return refcompteclient;
    }

    public void setRefcompteclient(Long refcompteclient) {
        this.refcompteclient = refcompteclient;
    }

    public BigInteger getPlafondencours() {
        return plafondencours;
    }

    public void setPlafondencours(BigInteger plafondencours) {
        this.plafondencours = plafondencours;
    }

    public Date getDatecreation() {
        return datecreation;
    }

    public void setDatecreation(Date datecreation) {
        this.datecreation = datecreation;
    }

    public Date getDatedernierupd() {
        return datedernierupd;
    }

    public void setDatedernierupd(Date datedernierupd) {
        this.datedernierupd = datedernierupd;
    }

    public Client getIdcontact() {
        return idcontact;
    }

    public void setIdcontact(Client idcontact) {
        this.idcontact = idcontact;
    }

    @XmlTransient
    public Collection<Facturevente> getFactureventeCollection() {
        return factureventeCollection;
    }

    public void setFactureventeCollection(Collection<Facturevente> factureventeCollection) {
        this.factureventeCollection = factureventeCollection;
    }

    @XmlTransient
    public Collection<Recouvrementclient> getRecouvrementclientCollection() {
        return recouvrementclientCollection;
    }

    public void setRecouvrementclientCollection(Collection<Recouvrementclient> recouvrementclientCollection) {
        this.recouvrementclientCollection = recouvrementclientCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refcompteclient != null ? refcompteclient.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Compteclient)) {
            return false;
        }
        Compteclient other = (Compteclient) object;
        if ((this.refcompteclient == null && other.refcompteclient != null) || (this.refcompteclient != null && !this.refcompteclient.equals(other.refcompteclient))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.MoonGardCRM.GestionProcessusVenteEntities.Compteclient[ refcompteclient=" + refcompteclient + " ]";
    }
    
}
