/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionRessourceCRUDSession;

import com.MoonGardCRM.GestionRessourceEntities.Vidange;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Jerbi
 */
@Local
public interface VidangeFacadeLocal {

    void create(Vidange vidange);

    Vidange edit(Vidange vidange);

    void remove(Vidange vidange);

    Vidange find(Object id);

    List<Vidange> findAll();

    List<Vidange> findRange(int[] range);

    int count();
    
}
