/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionProduitEntities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jerbi
 */
@Entity
@Table(name = "categorieproduit")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Categorieproduit.findAll", query = "SELECT c FROM Categorieproduit c"),
    @NamedQuery(name = "Categorieproduit.findByRefcategorieproduit", query = "SELECT c FROM Categorieproduit c WHERE c.refcategorieproduit = :refcategorieproduit"),
    @NamedQuery(name = "Categorieproduit.findByNomcategorie", query = "SELECT c FROM Categorieproduit c WHERE c.nomcategorie = :nomcategorie"),
    @NamedQuery(name = "Categorieproduit.findByDatecreation", query = "SELECT c FROM Categorieproduit c WHERE c.datecreation = :datecreation"),
    @NamedQuery(name = "Categorieproduit.findByDatemodification", query = "SELECT c FROM Categorieproduit c WHERE c.datemodification = :datemodification"),
    @NamedQuery(name = "Categorieproduit.findByEtat", query = "SELECT c FROM Categorieproduit c WHERE c.etat = :etat")})
public class Categorieproduit implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "refcategorieproduit")
    private Long refcategorieproduit;
    @Size(max = 254)
    @Column(name = "nomcategorie")
    private String nomcategorie;
    @Column(name = "datecreation")
    @Temporal(TemporalType.DATE)
    private Date datecreation;
    @Column(name = "datemodification")
    @Temporal(TemporalType.DATE)
    private Date datemodification;
    @Size(max = 254)
    @Column(name = "etat")
    private String etat;
    @OneToMany(mappedBy = "refcategorieproduit")
    private Collection<Produit> produitCollection;

    public Categorieproduit() {
    }

    public Categorieproduit(Long refcategorieproduit) {
        this.refcategorieproduit = refcategorieproduit;
    }

    public Long getRefcategorieproduit() {
        return refcategorieproduit;
    }

    public void setRefcategorieproduit(Long refcategorieproduit) {
        this.refcategorieproduit = refcategorieproduit;
    }

    public String getNomcategorie() {
        return nomcategorie;
    }

    public void setNomcategorie(String nomcategorie) {
        this.nomcategorie = nomcategorie;
    }

    public Date getDatecreation() {
        return datecreation;
    }

    public void setDatecreation(Date datecreation) {
        this.datecreation = datecreation;
    }

    public Date getDatemodification() {
        return datemodification;
    }

    public void setDatemodification(Date datemodification) {
        this.datemodification = datemodification;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    @XmlTransient
    public Collection<Produit> getProduitCollection() {
        return produitCollection;
    }

    public void setProduitCollection(Collection<Produit> produitCollection) {
        this.produitCollection = produitCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refcategorieproduit != null ? refcategorieproduit.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Categorieproduit)) {
            return false;
        }
        Categorieproduit other = (Categorieproduit) object;
        if ((this.refcategorieproduit == null && other.refcategorieproduit != null) || (this.refcategorieproduit != null && !this.refcategorieproduit.equals(other.refcategorieproduit))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.MoonGardCRM.GestionProduitEntities.Categorieproduit[ refcategorieproduit=" + refcategorieproduit + " ]";
    }
    
}
