/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionContactEntities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jerbi
 */
@Entity
@Table(name = "adressecontact")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Adressecontact.findAll", query = "SELECT a FROM Adressecontact a"),
    @NamedQuery(name = "Adressecontact.findByIdcontact", query = "SELECT a FROM Adressecontact a WHERE a.adressecontactPK.idcontact = :idcontact"),
    @NamedQuery(name = "Adressecontact.findByRefadressecontact", query = "SELECT a FROM Adressecontact a WHERE a.adressecontactPK.refadressecontact = :refadressecontact"),
    @NamedQuery(name = "Adressecontact.findByCommune", query = "SELECT a FROM Adressecontact a WHERE a.commune = :commune"),
    @NamedQuery(name = "Adressecontact.findByAdresse", query = "SELECT a FROM Adressecontact a WHERE a.adresse = :adresse"),
    @NamedQuery(name = "Adressecontact.findByCodepostal", query = "SELECT a FROM Adressecontact a WHERE a.codepostal = :codepostal")})
public class Adressecontact implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected AdressecontactPK adressecontactPK;
    @Size(max = 254)
    @Column(name = "commune")
    private String commune;
    @Size(max = 254)
    @Column(name = "adresse")
    private String adresse;
    @Size(max = 254)
    @Column(name = "codepostal")
    private String codepostal;
    @JoinColumn(name = "refregioncontact", referencedColumnName = "refregioncontact")
    @ManyToOne
    private Regioncontact refregioncontact;
    @JoinColumn(name = "idcontact", referencedColumnName = "idcontact", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Contact contact;

    public Adressecontact() {
    }

    public Adressecontact(AdressecontactPK adressecontactPK) {
        this.adressecontactPK = adressecontactPK;
    }

    public Adressecontact(long idcontact, long refadressecontact) {
        this.adressecontactPK = new AdressecontactPK(idcontact, refadressecontact);
    }

    public AdressecontactPK getAdressecontactPK() {
        return adressecontactPK;
    }

    public void setAdressecontactPK(AdressecontactPK adressecontactPK) {
        this.adressecontactPK = adressecontactPK;
    }

    public String getCommune() {
        return commune;
    }

    public void setCommune(String commune) {
        this.commune = commune;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getCodepostal() {
        return codepostal;
    }

    public void setCodepostal(String codepostal) {
        this.codepostal = codepostal;
    }

    public Regioncontact getRefregioncontact() {
        return refregioncontact;
    }

    public void setRefregioncontact(Regioncontact refregioncontact) {
        this.refregioncontact = refregioncontact;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (adressecontactPK != null ? adressecontactPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Adressecontact)) {
            return false;
        }
        Adressecontact other = (Adressecontact) object;
        if ((this.adressecontactPK == null && other.adressecontactPK != null) || (this.adressecontactPK != null && !this.adressecontactPK.equals(other.adressecontactPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.MoonGardCRM.GestionContactEntities.Adressecontact[ adressecontactPK=" + adressecontactPK + " ]";
    }
    
}
