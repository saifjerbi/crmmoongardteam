/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionProduitCRUDSessions;

import com.MoonGardCRM.GestionProduitEntities.Familleproduit;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Jerbi
 */
@Local
public interface FamilleproduitFacadeLocal {

    void create(Familleproduit familleproduit);

    Familleproduit edit(Familleproduit familleproduit);

    void remove(Familleproduit familleproduit);

    Familleproduit find(Object id);

    List<Familleproduit> findAll();

    List<Familleproduit> findRange(int[] range);

    int count();
    
}
