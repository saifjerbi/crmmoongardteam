/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionProcessusVenteEntities;

import com.MoonGardCRM.GestionContactEntities.Client;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jerbi
 */
@Entity
@Table(name = "commandevente")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Commandevente.findAll", query = "SELECT c FROM Commandevente c"),
    @NamedQuery(name = "Commandevente.findByNumerocom", query = "SELECT c FROM Commandevente c WHERE c.numerocom = :numerocom"),
    @NamedQuery(name = "Commandevente.findByDatecreation", query = "SELECT c FROM Commandevente c WHERE c.datecreation = :datecreation"),
    @NamedQuery(name = "Commandevente.findByTotalttc", query = "SELECT c FROM Commandevente c WHERE c.totalttc = :totalttc"),
    @NamedQuery(name = "Commandevente.findByTotalht", query = "SELECT c FROM Commandevente c WHERE c.totalht = :totalht"),
    @NamedQuery(name = "Commandevente.findByTotaltva", query = "SELECT c FROM Commandevente c WHERE c.totaltva = :totaltva"),
    @NamedQuery(name = "Commandevente.findByNetapayer", query = "SELECT c FROM Commandevente c WHERE c.netapayer = :netapayer"),
    @NamedQuery(name = "Commandevente.findByAcompte", query = "SELECT c FROM Commandevente c WHERE c.acompte = :acompte"),
    @NamedQuery(name = "Commandevente.findByResteapayer", query = "SELECT c FROM Commandevente c WHERE c.resteapayer = :resteapayer"),
    @NamedQuery(name = "Commandevente.findByMessageentete", query = "SELECT c FROM Commandevente c WHERE c.messageentete = :messageentete"),
    @NamedQuery(name = "Commandevente.findByNotebaspage", query = "SELECT c FROM Commandevente c WHERE c.notebaspage = :notebaspage"),
    @NamedQuery(name = "Commandevente.findByModelivraison", query = "SELECT c FROM Commandevente c WHERE c.modelivraison = :modelivraison"),
    @NamedQuery(name = "Commandevente.findByEtat", query = "SELECT c FROM Commandevente c WHERE c.etat = :etat")})
public class Commandevente implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "numerocom")
    private Long numerocom;
    @Column(name = "datecreation")
    @Temporal(TemporalType.DATE)
    private Date datecreation;
    @Column(name = "totalttc")
    private Integer totalttc;
    @Column(name = "totalht")
    private Integer totalht;
    @Column(name = "totaltva")
    private Integer totaltva;
    @Column(name = "netapayer")
    private Integer netapayer;
    @Column(name = "acompte")
    private BigInteger acompte;
    @Column(name = "resteapayer")
    private Integer resteapayer;
    @Size(max = 254)
    @Column(name = "messageentete")
    private String messageentete;
    @Size(max = 254)
    @Column(name = "notebaspage")
    private String notebaspage;
    @Size(max = 254)
    @Column(name = "modelivraison")
    private String modelivraison;
    @Size(max = 254)
    @Column(name = "etat")
    private String etat;
    @OneToMany(mappedBy = "numerocom")
    private Collection<Livraisonvente> livraisonventeCollection;
    @OneToMany(mappedBy = "numerocom")
    private Collection<Devisvente> devisventeCollection;
    @JoinColumn(name = "numerofact", referencedColumnName = "numerofact")
    @ManyToOne
    private Facturevente numerofact;
    @JoinColumn(name = "refcommercialvente", referencedColumnName = "refcommercialvente")
    @ManyToOne
    private Commercialvente refcommercialvente;
    @JoinColumn(name = "idcontact", referencedColumnName = "idcontact")
    @ManyToOne
    private Client idcontact;
    @OneToMany(mappedBy = "numerocom")
    private Collection<Lignecommandevente> lignecommandeventeCollection;

    public Commandevente() {
    }

    public Commandevente(Long numerocom) {
        this.numerocom = numerocom;
    }

    public Long getNumerocom() {
        return numerocom;
    }

    public void setNumerocom(Long numerocom) {
        this.numerocom = numerocom;
    }

    public Date getDatecreation() {
        return datecreation;
    }

    public void setDatecreation(Date datecreation) {
        this.datecreation = datecreation;
    }

    public Integer getTotalttc() {
        return totalttc;
    }

    public void setTotalttc(Integer totalttc) {
        this.totalttc = totalttc;
    }

    public Integer getTotalht() {
        return totalht;
    }

    public void setTotalht(Integer totalht) {
        this.totalht = totalht;
    }

    public Integer getTotaltva() {
        return totaltva;
    }

    public void setTotaltva(Integer totaltva) {
        this.totaltva = totaltva;
    }

    public Integer getNetapayer() {
        return netapayer;
    }

    public void setNetapayer(Integer netapayer) {
        this.netapayer = netapayer;
    }

    public BigInteger getAcompte() {
        return acompte;
    }

    public void setAcompte(BigInteger acompte) {
        this.acompte = acompte;
    }

    public Integer getResteapayer() {
        return resteapayer;
    }

    public void setResteapayer(Integer resteapayer) {
        this.resteapayer = resteapayer;
    }

    public String getMessageentete() {
        return messageentete;
    }

    public void setMessageentete(String messageentete) {
        this.messageentete = messageentete;
    }

    public String getNotebaspage() {
        return notebaspage;
    }

    public void setNotebaspage(String notebaspage) {
        this.notebaspage = notebaspage;
    }

    public String getModelivraison() {
        return modelivraison;
    }

    public void setModelivraison(String modelivraison) {
        this.modelivraison = modelivraison;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    @XmlTransient
    public Collection<Livraisonvente> getLivraisonventeCollection() {
        return livraisonventeCollection;
    }

    public void setLivraisonventeCollection(Collection<Livraisonvente> livraisonventeCollection) {
        this.livraisonventeCollection = livraisonventeCollection;
    }

    @XmlTransient
    public Collection<Devisvente> getDevisventeCollection() {
        return devisventeCollection;
    }

    public void setDevisventeCollection(Collection<Devisvente> devisventeCollection) {
        this.devisventeCollection = devisventeCollection;
    }

    public Facturevente getNumerofact() {
        return numerofact;
    }

    public void setNumerofact(Facturevente numerofact) {
        this.numerofact = numerofact;
    }

    public Commercialvente getRefcommercialvente() {
        return refcommercialvente;
    }

    public void setRefcommercialvente(Commercialvente refcommercialvente) {
        this.refcommercialvente = refcommercialvente;
    }

    public Client getIdcontact() {
        return idcontact;
    }

    public void setIdcontact(Client idcontact) {
        this.idcontact = idcontact;
    }

    @XmlTransient
    public Collection<Lignecommandevente> getLignecommandeventeCollection() {
        return lignecommandeventeCollection;
    }

    public void setLignecommandeventeCollection(Collection<Lignecommandevente> lignecommandeventeCollection) {
        this.lignecommandeventeCollection = lignecommandeventeCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (numerocom != null ? numerocom.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Commandevente)) {
            return false;
        }
        Commandevente other = (Commandevente) object;
        if ((this.numerocom == null && other.numerocom != null) || (this.numerocom != null && !this.numerocom.equals(other.numerocom))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.MoonGardCRM.GestionProcessusVenteEntities.Commandevente[ numerocom=" + numerocom + " ]";
    }
    
}
