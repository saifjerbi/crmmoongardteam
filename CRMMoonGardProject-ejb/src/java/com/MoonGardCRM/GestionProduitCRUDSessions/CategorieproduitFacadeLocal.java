/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionProduitCRUDSessions;

import com.MoonGardCRM.GestionProduitEntities.Categorieproduit;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Jerbi
 */
@Local
public interface CategorieproduitFacadeLocal {

    void create(Categorieproduit categorieproduit);

    Categorieproduit edit(Categorieproduit categorieproduit);

    void remove(Categorieproduit categorieproduit);

    Categorieproduit find(Object id);

    List<Categorieproduit> findAll();

    List<Categorieproduit> findRange(int[] range);

    int count();
    
}
