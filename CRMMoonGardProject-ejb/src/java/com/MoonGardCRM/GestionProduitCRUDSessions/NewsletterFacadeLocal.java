/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionProduitCRUDSessions;

import com.MoonGardCRM.GestionProduitEntities.Newsletter;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Jerbi
 */
@Local
public interface NewsletterFacadeLocal {

    void create(Newsletter newsletter);

    Newsletter edit(Newsletter newsletter);

    void remove(Newsletter newsletter);

    Newsletter find(Object id);

    List<Newsletter> findAll();

    List<Newsletter> findRange(int[] range);

    int count();
    
}
