/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionRessourceCRUDSession;

import com.MoonGardCRM.CRUDAbstractFacade.AbstractFacade;
import com.MoonGardCRM.GestionRessourceEntities.Piecederechange;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Jerbi
 */
@Stateless
public class PiecederechangeFacade extends AbstractFacade<Piecederechange> implements PiecederechangeFacadeLocal {
    @PersistenceContext(unitName = "CRMMoonGardProject-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PiecederechangeFacade() {
        super(Piecederechange.class);
    }
    
}
