/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionContactCRUDSessions;

import com.MoonGardCRM.GestionContactEntities.Adressecontact;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Jerbi
 */
@Local
public interface AdressecontactFacadeLocal {

    void create(Adressecontact adressecontact);

    Adressecontact edit(Adressecontact adressecontact);

    void remove(Adressecontact adressecontact);

    Adressecontact find(Object id);

    List<Adressecontact> findAll();

    List<Adressecontact> findRange(int[] range);

    int count();
    
}
