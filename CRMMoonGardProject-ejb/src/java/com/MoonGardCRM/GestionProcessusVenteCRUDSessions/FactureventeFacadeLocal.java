/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionProcessusVenteCRUDSessions;

import com.MoonGardCRM.GestionProcessusVenteEntities.Facturevente;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Jerbi
 */
@Local
public interface FactureventeFacadeLocal {

    void create(Facturevente facturevente);

    Facturevente edit(Facturevente facturevente);

    void remove(Facturevente facturevente);

    Facturevente find(Object id);

    List<Facturevente> findAll();

    List<Facturevente> findRange(int[] range);

    int count();
    
}
