/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionRessourceCRUDSession;

import com.MoonGardCRM.GestionRessourceEntities.Lavage;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Jerbi
 */
@Local
public interface LavageFacadeLocal {

    void create(Lavage lavage);

    Lavage edit(Lavage lavage);

    void remove(Lavage lavage);

    Lavage find(Object id);

    List<Lavage> findAll();

    List<Lavage> findRange(int[] range);

    int count();
    
}
