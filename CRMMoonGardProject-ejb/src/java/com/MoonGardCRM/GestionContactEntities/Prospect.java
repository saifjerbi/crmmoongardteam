/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionContactEntities;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jerbi
 */
@Entity
@Table(name = "prospect")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Prospect.findAll", query = "SELECT p FROM Prospect p"),
    @NamedQuery(name = "Prospect.findByIdcontact", query = "SELECT p FROM Prospect p WHERE p.idcontact = :idcontact"),
    @NamedQuery(name = "Prospect.findByOriginecontact", query = "SELECT p FROM Prospect p WHERE p.originecontact = :originecontact"),
    @NamedQuery(name = "Prospect.findByCumulca", query = "SELECT p FROM Prospect p WHERE p.cumulca = :cumulca"),
    @NamedQuery(name = "Prospect.findByCumulcasolde", query = "SELECT p FROM Prospect p WHERE p.cumulcasolde = :cumulcasolde")})
public class Prospect implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "idcontact")
    private Long idcontact;
    @Size(max = 254)
    @Column(name = "originecontact")
    private String originecontact;
    @Column(name = "cumulca")
    private BigInteger cumulca;
    @Column(name = "cumulcasolde")
    private BigInteger cumulcasolde;
    @JoinColumn(name = "idcontact", referencedColumnName = "idcontact", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Contact contact;

    public Prospect() {
    }

    public Prospect(Long idcontact) {
        this.idcontact = idcontact;
    }

    public Long getIdcontact() {
        return idcontact;
    }

    public void setIdcontact(Long idcontact) {
        this.idcontact = idcontact;
    }

    public String getOriginecontact() {
        return originecontact;
    }

    public void setOriginecontact(String originecontact) {
        this.originecontact = originecontact;
    }

    public BigInteger getCumulca() {
        return cumulca;
    }

    public void setCumulca(BigInteger cumulca) {
        this.cumulca = cumulca;
    }

    public BigInteger getCumulcasolde() {
        return cumulcasolde;
    }

    public void setCumulcasolde(BigInteger cumulcasolde) {
        this.cumulcasolde = cumulcasolde;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idcontact != null ? idcontact.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Prospect)) {
            return false;
        }
        Prospect other = (Prospect) object;
        if ((this.idcontact == null && other.idcontact != null) || (this.idcontact != null && !this.idcontact.equals(other.idcontact))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.MoonGardCRM.GestionContactEntities.Prospect[ idcontact=" + idcontact + " ]";
    }
    
}
