/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionProcessusVenteCRUDSessions;

import com.MoonGardCRM.GestionProcessusVenteEntities.Commandevente;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Jerbi
 */
@Local
public interface CommandeventeFacadeLocal {

    void create(Commandevente commandevente);

    Commandevente edit(Commandevente commandevente);

    void remove(Commandevente commandevente);

    Commandevente find(Object id);

    List<Commandevente> findAll();

    List<Commandevente> findRange(int[] range);

    int count();
    
}
