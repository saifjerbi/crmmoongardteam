/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionProcessusVenteCRUDSessions;

import com.MoonGardCRM.GestionProcessusVenteEntities.Entreestockfactureavoirclient;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Jerbi
 */
@Local
public interface EntreestockfactureavoirclientFacadeLocal {

    void create(Entreestockfactureavoirclient entreestockfactureavoirclient);

    Entreestockfactureavoirclient edit(Entreestockfactureavoirclient entreestockfactureavoirclient);

    void remove(Entreestockfactureavoirclient entreestockfactureavoirclient);

    Entreestockfactureavoirclient find(Object id);

    List<Entreestockfactureavoirclient> findAll();

    List<Entreestockfactureavoirclient> findRange(int[] range);

    int count();
    
}
