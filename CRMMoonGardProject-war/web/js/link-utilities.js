(function($) {
	$.fn.linkUtilities = function(params) {
    	$('a').not('.original, .submit, #contextMenu li a:not(.confirmSupp), .operator_status_button').each(function(){
			$(this).find('a.confirmSupp span').css('display','none');
			var title = $(this).attr('title');
			var target = $(this).attr('target');
    		var href = $(this).attr('href');		// on r�cup�re le lien
    		$(this).bind('click',function() {
	    		var isFound = $(this).hasClass('confirmSupp');
				var parent = $(this).hasClass('parent');
				var javascript = $(this).hasClass('javascript_content');
	    		if (isFound == true) {
	    			var dialog_window = '<div id=\"dialog-message\" title=\"\" style=\"display:none;\"><p><span class=\"ui-icon ui-icon-alert\" style=\"margin:0 7px 20px 0;\"></span><span id=\"dialog-message-content\"></span></p></div>';
	    			$('body').append(dialog_window);
    				var description = $(this).find('span.content').html();
    				$('#dialog-message').attr('title',title);
    				$('#dialog-message-content').html(description);
    				var buttonsOpts = {};
    				var btn1 = $(this).find('span.btn1').html();
    				var btn2 = $(this).find('span.btn2').html();
    				if (btn1 != null) {
    					buttonsOpts[btn1] = function() {
    						if (javascript == true) window[href]();
    						else if (parent == true) window.parent.parseLink(href);	// on traite le lien
		    				else parseLink(href);
			    			return false;		// on annule le click du lien
						}
    				}
    				if (btn2 != null) {
    					buttonsOpts[btn2] = function() {
							$( this ).dialog( "close" );
							$('dialog-confirm-supp').remove();
							return false;
						}
    				}
    				$('#dialog-message').dialog({
    					resizable: false,
    					modal: true,
    					buttons: buttonsOpts
    				});
    				return false;
	    		} else {
    				if (parent == true) window.parent.parseLink(href);	// on traite le lien
    				else parseLink(href);
	    			return false;		// on annule le click du lien
	    		}
    		});
	    	
    		parseLink = function(href) {
    		// le mod�le pour la r�cup�ration de l'url compl�te du type: https://SOUSDOMAINE.DOMAINE.EXTENSION/PAGE.php
	    		//var model_link = /https:\/\/([www\.]+\.)?([-0-9A-Za-z_]+(\.[a-z-A-Z]+\.)+([a-z-A-Z]{2,4})+(\/[a-z-A-Z-_]+\.)+([a-z-A-Z]{3,4})?)/;
    			var model_link = /https:\/\/([www\.]+\.)?([-0-9A-Za-z_]+(\.[a-z-A-Z]+\.)+([a-z-A-Z-0-9:]{2,8})+(\/[a-z-A-Z-_]+\.)+([a-z-A-Z]{3,4})?)/;
	    		if (wwwroot = model_link.exec(href)) {	// si lien corresponds on continue
	    			var link = wwwroot[0];
	    			$(this).attr('href',link);
	    			var parametres = href.replace(link,'');
	    			if (parametres.search('/?/')) {		// on v�rifie si des variables sont pr�sentes gr�ce au ? situ� apr�s .php
	    				var form = '<form action="' + link +'" method="post" id="generate-form">';		// on g�n�re un <form>
		    			parametres = parametres.replace('?','');				// on efface le ?
		    			var parametres_list = parametres.split('&');			// on d�coupe tous les &
		    			var total_parametres = parametres_list.length;
		    			if (total_parametres > 0) {
		    				for (i=0;i<total_parametres;i++) {		// on traite les variables
		    					nom_parametre = /([-0-9A-Za-z_.-]+)/.exec(parametres_list[i]);			// nom de la variable
		    					if (value_parametre = /\=([-0-9A-Za-z_.-]+)/.exec(parametres_list[i]))		// valeur de la variable
		    						form += '<input type="hidden" name="' + nom_parametre[0] + '" value="' + value_parametre[1] + '" />';	// on g�n�re le champ input[type=hidden]
		    				}
		    			}
		    			form += '</form>';
		    			$('body').append(form);				// on ajoute le formulaire � la page
		    			$('#generate-form').submit();		// on valide le formulaire
		    			return false;		// on annule le click du lien
	    			} else return true;		// si pas de variable, on laisse le lien faire sa fonction
	    		} else return true;			// si lien non reconnu, on laisse le lien faire sa fonction
			}
    	});
    	return $(this); 
	}
})(jQuery);