/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionRessourceCRUDSession;

import com.MoonGardCRM.GestionRessourceEntities.Piecederechange;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Jerbi
 */
@Local
public interface PiecederechangeFacadeLocal {

    void create(Piecederechange piecederechange);

    Piecederechange edit(Piecederechange piecederechange);

    void remove(Piecederechange piecederechange);

    Piecederechange find(Object id);

    List<Piecederechange> findAll();

    List<Piecederechange> findRange(int[] range);

    int count();
    
}
