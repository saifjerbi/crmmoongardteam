/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionProduitEntities;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jerbi
 */
@Entity
@Table(name = "tarifproduit")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tarifproduit.findAll", query = "SELECT t FROM Tarifproduit t"),
    @NamedQuery(name = "Tarifproduit.findByReftarifproduit", query = "SELECT t FROM Tarifproduit t WHERE t.reftarifproduit = :reftarifproduit"),
    @NamedQuery(name = "Tarifproduit.findByPrixachat", query = "SELECT t FROM Tarifproduit t WHERE t.prixachat = :prixachat"),
    @NamedQuery(name = "Tarifproduit.findByPrixunitaire1", query = "SELECT t FROM Tarifproduit t WHERE t.prixunitaire1 = :prixunitaire1"),
    @NamedQuery(name = "Tarifproduit.findByPrixunitaire2", query = "SELECT t FROM Tarifproduit t WHERE t.prixunitaire2 = :prixunitaire2"),
    @NamedQuery(name = "Tarifproduit.findByPrixunitaire3", query = "SELECT t FROM Tarifproduit t WHERE t.prixunitaire3 = :prixunitaire3"),
    @NamedQuery(name = "Tarifproduit.findByPrixunitaire4", query = "SELECT t FROM Tarifproduit t WHERE t.prixunitaire4 = :prixunitaire4"),
    @NamedQuery(name = "Tarifproduit.findByPrixensolde", query = "SELECT t FROM Tarifproduit t WHERE t.prixensolde = :prixensolde"),
    @NamedQuery(name = "Tarifproduit.findByMargebenefice", query = "SELECT t FROM Tarifproduit t WHERE t.margebenefice = :margebenefice"),
    @NamedQuery(name = "Tarifproduit.findByLimiteremise", query = "SELECT t FROM Tarifproduit t WHERE t.limiteremise = :limiteremise"),
    @NamedQuery(name = "Tarifproduit.findByTauxtva", query = "SELECT t FROM Tarifproduit t WHERE t.tauxtva = :tauxtva"),
    @NamedQuery(name = "Tarifproduit.findByValeurtva", query = "SELECT t FROM Tarifproduit t WHERE t.valeurtva = :valeurtva")})
public class Tarifproduit implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "reftarifproduit")
    private Long reftarifproduit;
    @Column(name = "prixachat")
    private BigInteger prixachat;
    @Column(name = "prixunitaire1")
    private BigInteger prixunitaire1;
    @Column(name = "prixunitaire2")
    private BigInteger prixunitaire2;
    @Column(name = "prixunitaire3")
    private BigInteger prixunitaire3;
    @Column(name = "prixunitaire4")
    private BigInteger prixunitaire4;
    @Column(name = "prixensolde")
    private BigInteger prixensolde;
    @Column(name = "margebenefice")
    private BigInteger margebenefice;
    @Column(name = "limiteremise")
    private BigInteger limiteremise;
    @Column(name = "tauxtva")
    private BigInteger tauxtva;
    @Column(name = "valeurtva")
    private BigInteger valeurtva;
    @OneToMany(mappedBy = "reftarifproduit")
    private Collection<Produit> produitCollection;

    public Tarifproduit() {
    }

    public Tarifproduit(Long reftarifproduit) {
        this.reftarifproduit = reftarifproduit;
    }

    public Long getReftarifproduit() {
        return reftarifproduit;
    }

    public void setReftarifproduit(Long reftarifproduit) {
        this.reftarifproduit = reftarifproduit;
    }

    public BigInteger getPrixachat() {
        return prixachat;
    }

    public void setPrixachat(BigInteger prixachat) {
        this.prixachat = prixachat;
    }

    public BigInteger getPrixunitaire1() {
        return prixunitaire1;
    }

    public void setPrixunitaire1(BigInteger prixunitaire1) {
        this.prixunitaire1 = prixunitaire1;
    }

    public BigInteger getPrixunitaire2() {
        return prixunitaire2;
    }

    public void setPrixunitaire2(BigInteger prixunitaire2) {
        this.prixunitaire2 = prixunitaire2;
    }

    public BigInteger getPrixunitaire3() {
        return prixunitaire3;
    }

    public void setPrixunitaire3(BigInteger prixunitaire3) {
        this.prixunitaire3 = prixunitaire3;
    }

    public BigInteger getPrixunitaire4() {
        return prixunitaire4;
    }

    public void setPrixunitaire4(BigInteger prixunitaire4) {
        this.prixunitaire4 = prixunitaire4;
    }

    public BigInteger getPrixensolde() {
        return prixensolde;
    }

    public void setPrixensolde(BigInteger prixensolde) {
        this.prixensolde = prixensolde;
    }

    public BigInteger getMargebenefice() {
        return margebenefice;
    }

    public void setMargebenefice(BigInteger margebenefice) {
        this.margebenefice = margebenefice;
    }

    public BigInteger getLimiteremise() {
        return limiteremise;
    }

    public void setLimiteremise(BigInteger limiteremise) {
        this.limiteremise = limiteremise;
    }

    public BigInteger getTauxtva() {
        return tauxtva;
    }

    public void setTauxtva(BigInteger tauxtva) {
        this.tauxtva = tauxtva;
    }

    public BigInteger getValeurtva() {
        return valeurtva;
    }

    public void setValeurtva(BigInteger valeurtva) {
        this.valeurtva = valeurtva;
    }

    @XmlTransient
    public Collection<Produit> getProduitCollection() {
        return produitCollection;
    }

    public void setProduitCollection(Collection<Produit> produitCollection) {
        this.produitCollection = produitCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (reftarifproduit != null ? reftarifproduit.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tarifproduit)) {
            return false;
        }
        Tarifproduit other = (Tarifproduit) object;
        if ((this.reftarifproduit == null && other.reftarifproduit != null) || (this.reftarifproduit != null && !this.reftarifproduit.equals(other.reftarifproduit))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.MoonGardCRM.GestionProduitEntities.Tarifproduit[ reftarifproduit=" + reftarifproduit + " ]";
    }
    
}
