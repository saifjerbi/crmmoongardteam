/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionMarketingEntities;

import com.MoonGardCRM.GestionContactEntities.Contact;
import com.MoonGardCRM.GestionProduitEntities.Newsletter;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jerbi
 */
@Entity
@Table(name = "newslettercontact")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Newslettercontact.findAll", query = "SELECT n FROM Newslettercontact n"),
    @NamedQuery(name = "Newslettercontact.findByRefnewsletter", query = "SELECT n FROM Newslettercontact n WHERE n.newslettercontactPK.refnewsletter = :refnewsletter"),
    @NamedQuery(name = "Newslettercontact.findByIdcontact", query = "SELECT n FROM Newslettercontact n WHERE n.newslettercontactPK.idcontact = :idcontact"),
    @NamedQuery(name = "Newslettercontact.findByDateenvoi", query = "SELECT n FROM Newslettercontact n WHERE n.dateenvoi = :dateenvoi")})
public class Newslettercontact implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected NewslettercontactPK newslettercontactPK;
    @Column(name = "dateenvoi")
    @Temporal(TemporalType.DATE)
    private Date dateenvoi;
    @JoinColumn(name = "refnewsletter", referencedColumnName = "refnewsletter", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Newsletter newsletter;
    @JoinColumn(name = "idcontact", referencedColumnName = "idcontact", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Contact contact;

    public Newslettercontact() {
    }

    public Newslettercontact(NewslettercontactPK newslettercontactPK) {
        this.newslettercontactPK = newslettercontactPK;
    }

    public Newslettercontact(long refnewsletter, long idcontact) {
        this.newslettercontactPK = new NewslettercontactPK(refnewsletter, idcontact);
    }

    public NewslettercontactPK getNewslettercontactPK() {
        return newslettercontactPK;
    }

    public void setNewslettercontactPK(NewslettercontactPK newslettercontactPK) {
        this.newslettercontactPK = newslettercontactPK;
    }

    public Date getDateenvoi() {
        return dateenvoi;
    }

    public void setDateenvoi(Date dateenvoi) {
        this.dateenvoi = dateenvoi;
    }

    public Newsletter getNewsletter() {
        return newsletter;
    }

    public void setNewsletter(Newsletter newsletter) {
        this.newsletter = newsletter;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (newslettercontactPK != null ? newslettercontactPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Newslettercontact)) {
            return false;
        }
        Newslettercontact other = (Newslettercontact) object;
        if ((this.newslettercontactPK == null && other.newslettercontactPK != null) || (this.newslettercontactPK != null && !this.newslettercontactPK.equals(other.newslettercontactPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.MoonGardCRM.GestionMarketingEntities.Newslettercontact[ newslettercontactPK=" + newslettercontactPK + " ]";
    }
    
}
