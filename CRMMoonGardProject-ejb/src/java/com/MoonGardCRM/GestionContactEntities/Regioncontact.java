/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionContactEntities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jerbi
 */
@Entity
@Table(name = "regioncontact")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Regioncontact.findAll", query = "SELECT r FROM Regioncontact r"),
    @NamedQuery(name = "Regioncontact.findByRefregioncontact", query = "SELECT r FROM Regioncontact r WHERE r.refregioncontact = :refregioncontact"),
    @NamedQuery(name = "Regioncontact.findByNom", query = "SELECT r FROM Regioncontact r WHERE r.nom = :nom")})
public class Regioncontact implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "refregioncontact")
    private Long refregioncontact;
    @Size(max = 254)
    @Column(name = "nom")
    private String nom;
    @JoinColumn(name = "refpays", referencedColumnName = "refpays")
    @ManyToOne
    private Pays refpays;
    @OneToMany(mappedBy = "refregioncontact")
    private Collection<Adressecontact> adressecontactCollection;

    public Regioncontact() {
    }

    public Regioncontact(Long refregioncontact) {
        this.refregioncontact = refregioncontact;
    }

    public Long getRefregioncontact() {
        return refregioncontact;
    }

    public void setRefregioncontact(Long refregioncontact) {
        this.refregioncontact = refregioncontact;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Pays getRefpays() {
        return refpays;
    }

    public void setRefpays(Pays refpays) {
        this.refpays = refpays;
    }

    @XmlTransient
    public Collection<Adressecontact> getAdressecontactCollection() {
        return adressecontactCollection;
    }

    public void setAdressecontactCollection(Collection<Adressecontact> adressecontactCollection) {
        this.adressecontactCollection = adressecontactCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refregioncontact != null ? refregioncontact.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Regioncontact)) {
            return false;
        }
        Regioncontact other = (Regioncontact) object;
        if ((this.refregioncontact == null && other.refregioncontact != null) || (this.refregioncontact != null && !this.refregioncontact.equals(other.refregioncontact))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.MoonGardCRM.GestionContactEntities.Regioncontact[ refregioncontact=" + refregioncontact + " ]";
    }
    
}
