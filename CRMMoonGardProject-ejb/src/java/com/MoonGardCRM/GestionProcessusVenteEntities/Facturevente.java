/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionProcessusVenteEntities;

import com.MoonGardCRM.GestionContactEntities.Client;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jerbi
 */
@Entity
@Table(name = "facturevente")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Facturevente.findAll", query = "SELECT f FROM Facturevente f"),
    @NamedQuery(name = "Facturevente.findByNumerofact", query = "SELECT f FROM Facturevente f WHERE f.numerofact = :numerofact"),
    @NamedQuery(name = "Facturevente.findByDatecreation", query = "SELECT f FROM Facturevente f WHERE f.datecreation = :datecreation"),
    @NamedQuery(name = "Facturevente.findByTotalttc", query = "SELECT f FROM Facturevente f WHERE f.totalttc = :totalttc"),
    @NamedQuery(name = "Facturevente.findByTotalht", query = "SELECT f FROM Facturevente f WHERE f.totalht = :totalht"),
    @NamedQuery(name = "Facturevente.findByTotaltva", query = "SELECT f FROM Facturevente f WHERE f.totaltva = :totaltva"),
    @NamedQuery(name = "Facturevente.findByNetdejapayer", query = "SELECT f FROM Facturevente f WHERE f.netdejapayer = :netdejapayer"),
    @NamedQuery(name = "Facturevente.findByNetapayer", query = "SELECT f FROM Facturevente f WHERE f.netapayer = :netapayer"),
    @NamedQuery(name = "Facturevente.findByEcheance", query = "SELECT f FROM Facturevente f WHERE f.echeance = :echeance"),
    @NamedQuery(name = "Facturevente.findByMessageentete", query = "SELECT f FROM Facturevente f WHERE f.messageentete = :messageentete"),
    @NamedQuery(name = "Facturevente.findByNotebaspage", query = "SELECT f FROM Facturevente f WHERE f.notebaspage = :notebaspage"),
    @NamedQuery(name = "Facturevente.findByModelivraison", query = "SELECT f FROM Facturevente f WHERE f.modelivraison = :modelivraison"),
    @NamedQuery(name = "Facturevente.findByModepayement", query = "SELECT f FROM Facturevente f WHERE f.modepayement = :modepayement"),
    @NamedQuery(name = "Facturevente.findByEtat", query = "SELECT f FROM Facturevente f WHERE f.etat = :etat")})
public class Facturevente implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "numerofact")
    private Long numerofact;
    @Column(name = "datecreation")
    @Temporal(TemporalType.DATE)
    private Date datecreation;
    @Column(name = "totalttc")
    private BigInteger totalttc;
    @Column(name = "totalht")
    private BigInteger totalht;
    @Column(name = "totaltva")
    private BigInteger totaltva;
    @Column(name = "netdejapayer")
    private BigInteger netdejapayer;
    @Column(name = "netapayer")
    private BigInteger netapayer;
    @Column(name = "echeance")
    @Temporal(TemporalType.DATE)
    private Date echeance;
    @Size(max = 254)
    @Column(name = "messageentete")
    private String messageentete;
    @Size(max = 254)
    @Column(name = "notebaspage")
    private String notebaspage;
    @Size(max = 254)
    @Column(name = "modelivraison")
    private String modelivraison;
    @Size(max = 254)
    @Column(name = "modepayement")
    private String modepayement;
    @Size(max = 254)
    @Column(name = "etat")
    private String etat;
    @OneToMany(mappedBy = "numerofact")
    private Collection<Livraisonvente> livraisonventeCollection;
    @OneToMany(mappedBy = "numerofact")
    private Collection<Payementfacturevente> payementfactureventeCollection;
    @JoinColumn(name = "refcompteclient", referencedColumnName = "refcompteclient")
    @ManyToOne
    private Compteclient refcompteclient;
    @JoinColumn(name = "idcontact", referencedColumnName = "idcontact")
    @ManyToOne
    private Client idcontact;
    @OneToMany(mappedBy = "numerofact")
    private Collection<Lignefacturevente> lignefactureventeCollection;
    @OneToMany(mappedBy = "numerofact")
    private Collection<Commandevente> commandeventeCollection;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "facturevente")
    private Factureavoirvente factureavoirvente;

    public Facturevente() {
    }

    public Facturevente(Long numerofact) {
        this.numerofact = numerofact;
    }

    public Long getNumerofact() {
        return numerofact;
    }

    public void setNumerofact(Long numerofact) {
        this.numerofact = numerofact;
    }

    public Date getDatecreation() {
        return datecreation;
    }

    public void setDatecreation(Date datecreation) {
        this.datecreation = datecreation;
    }

    public BigInteger getTotalttc() {
        return totalttc;
    }

    public void setTotalttc(BigInteger totalttc) {
        this.totalttc = totalttc;
    }

    public BigInteger getTotalht() {
        return totalht;
    }

    public void setTotalht(BigInteger totalht) {
        this.totalht = totalht;
    }

    public BigInteger getTotaltva() {
        return totaltva;
    }

    public void setTotaltva(BigInteger totaltva) {
        this.totaltva = totaltva;
    }

    public BigInteger getNetdejapayer() {
        return netdejapayer;
    }

    public void setNetdejapayer(BigInteger netdejapayer) {
        this.netdejapayer = netdejapayer;
    }

    public BigInteger getNetapayer() {
        return netapayer;
    }

    public void setNetapayer(BigInteger netapayer) {
        this.netapayer = netapayer;
    }

    public Date getEcheance() {
        return echeance;
    }

    public void setEcheance(Date echeance) {
        this.echeance = echeance;
    }

    public String getMessageentete() {
        return messageentete;
    }

    public void setMessageentete(String messageentete) {
        this.messageentete = messageentete;
    }

    public String getNotebaspage() {
        return notebaspage;
    }

    public void setNotebaspage(String notebaspage) {
        this.notebaspage = notebaspage;
    }

    public String getModelivraison() {
        return modelivraison;
    }

    public void setModelivraison(String modelivraison) {
        this.modelivraison = modelivraison;
    }

    public String getModepayement() {
        return modepayement;
    }

    public void setModepayement(String modepayement) {
        this.modepayement = modepayement;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    @XmlTransient
    public Collection<Livraisonvente> getLivraisonventeCollection() {
        return livraisonventeCollection;
    }

    public void setLivraisonventeCollection(Collection<Livraisonvente> livraisonventeCollection) {
        this.livraisonventeCollection = livraisonventeCollection;
    }

    @XmlTransient
    public Collection<Payementfacturevente> getPayementfactureventeCollection() {
        return payementfactureventeCollection;
    }

    public void setPayementfactureventeCollection(Collection<Payementfacturevente> payementfactureventeCollection) {
        this.payementfactureventeCollection = payementfactureventeCollection;
    }

    public Compteclient getRefcompteclient() {
        return refcompteclient;
    }

    public void setRefcompteclient(Compteclient refcompteclient) {
        this.refcompteclient = refcompteclient;
    }

    public Client getIdcontact() {
        return idcontact;
    }

    public void setIdcontact(Client idcontact) {
        this.idcontact = idcontact;
    }

    @XmlTransient
    public Collection<Lignefacturevente> getLignefactureventeCollection() {
        return lignefactureventeCollection;
    }

    public void setLignefactureventeCollection(Collection<Lignefacturevente> lignefactureventeCollection) {
        this.lignefactureventeCollection = lignefactureventeCollection;
    }

    @XmlTransient
    public Collection<Commandevente> getCommandeventeCollection() {
        return commandeventeCollection;
    }

    public void setCommandeventeCollection(Collection<Commandevente> commandeventeCollection) {
        this.commandeventeCollection = commandeventeCollection;
    }

    public Factureavoirvente getFactureavoirvente() {
        return factureavoirvente;
    }

    public void setFactureavoirvente(Factureavoirvente factureavoirvente) {
        this.factureavoirvente = factureavoirvente;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (numerofact != null ? numerofact.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Facturevente)) {
            return false;
        }
        Facturevente other = (Facturevente) object;
        if ((this.numerofact == null && other.numerofact != null) || (this.numerofact != null && !this.numerofact.equals(other.numerofact))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.MoonGardCRM.GestionProcessusVenteEntities.Facturevente[ numerofact=" + numerofact + " ]";
    }
    
}
