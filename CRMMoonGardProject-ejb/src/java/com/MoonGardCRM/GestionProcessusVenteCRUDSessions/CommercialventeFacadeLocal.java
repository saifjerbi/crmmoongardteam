/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionProcessusVenteCRUDSessions;

import com.MoonGardCRM.GestionProcessusVenteEntities.Commercialvente;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Jerbi
 */
@Local
public interface CommercialventeFacadeLocal {

    void create(Commercialvente commercialvente);

    Commercialvente edit(Commercialvente commercialvente);

    void remove(Commercialvente commercialvente);

    Commercialvente find(Object id);

    List<Commercialvente> findAll();

    List<Commercialvente> findRange(int[] range);

    int count();
    
}
