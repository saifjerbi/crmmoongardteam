function envoieRequete(url,id) {
	document.getElementById(id).innerHTML = "<img src='./img/loader.gif' alt='Chargement en cours...' />";
	var xhr_object = null;
	var position = id;
	if (window.XMLHttpRequest) xhr_object = new XMLHttpRequest();
	else if (window.ActiveXObject) xhr_object = new ActiveXObject("Microsoft.XMLHTTP");
	xhr_object.open("GET", url, true);
	xhr_object.onreadystatechange = function() {
		if (xhr_object.readyState == 4 ) {
			document.getElementById(position).innerHTML = xhr_object.responseText;
		}
	}
	xhr_object.send(null);
}
function AddShortCode(code) {
	var content = $('#entreprise_signature').val() + code;
	$('#entreprise_signature').val(content).focus();
}
function AddShortCode2(code) {
	var content = $('#message').getCode() + code;
	$('#message').setCode(content).setFocus();
}
var heightMenu = function () {
	var resolution = parseInt(jQuery(window).height()) - 200;
	var contenu_height = $('#contenu').height();
	var menu_height = $('#menu').height();
	if ((menu_height < resolution) && (contenu_height < resolution)) $('#menu, #contenu').height(resolution);
	else if (contenu_height > menu_height) $('#menu').height(contenu_height);
	else $('#contenu').height(menu_height);
}
var ShowHomeNews = function(limite) {
	$('#nouveautes-home').load(
		'/include/ajax_astuces.php',
		{'limite': limite}
	);
}
var UItabsResize = function(selector) {
	if ($(selector + ' ul.ui-widget-header').height() > 30) {
		var tabs_position = $(selector + ' ul.ui-widget-header').position().top;
		var resize_tabs = 0;
		$(selector + ' ul.ui-widget-header li').each(function(){ if ($(this).position().top != tabs_position) resize_tabs = 1; });
		if (resize_tabs == 1) {
			var nb_onglet = $(selector + ' li').length;
			var tabs_width = $(selector + ' ul.ui-widget-header').width() - 50;
			var largeur_li = parseInt(tabs_width / nb_onglet);
			$(selector + ' ul.ui-widget-header li').each(function(){
				if (largeur_li < $(this).width()) {
					$(this).css({
						'width': largeur_li + 'px'
					});
					$(this).find('a').css({
						'width': (largeur_li - 15) + 'px',
						'display': 'block',
						'overflow': 'hidden',
						'text-overflow': 'ellipsis',
						'white-space': 'nowrap'
					}).addClass('tooltip');
					$(this).find('a').tooltip({position: {my: 'center bottom-5', at: 'center top'}, tooltipClass: 'custom-tooltip-white'});
				}
			});
		}
	}
}
function HideHelpContent(id_help, wwwroot) {
	$('#help-block-' + id_help).fadeOut(800);
	var data_post = {};
	data_post['id'] = id_help;
	$.ajax({
		type: "POST",
		url: wwwroot + "/include/ajax_help.php",
		data: data_post
	});
}
function HideHomeBlock(id_astuce, wwwroot) {
	$('#home-block-' + id_astuce).fadeOut(800);
	var data_post = {};
	data_post['id'] = id_astuce;
	$.ajax({
		type: "POST",
		url: wwwroot + "/include/ajax_astuces.php",
		data: data_post
	});
}
function Popup(mypage,titre,w,h,scroll) {
	var LeftPosition = (screen.width-w)/2;
	var TopPosition = (screen.height-h)/2;
	var settings = 'height='+h+',width='+w+',top='+TopPosition+',left='+LeftPosition+',menubar=no,location=no,scrollbars=no,resizable=no';
	window.open(mypage, titre, settings);
}
function supports_canvas() {
	return !!document.createElement('canvas').getContext;
}
function transfert(src, dest) {
	if (src.options[src.selectedIndex].value == 0) return;

	var nouveau = new Option(src.options[src.selectedIndex].text, src.options[src.selectedIndex].value);
	dest.options[dest.length] = nouveau;
	src.options[src.selectedIndex] = null;
}
function transfertSelect(src, dest) {
	if ((!$('#' + src + ' option:selected').length) || ($('#' + src + ' option:selected').val() == 0) || ($('#' + src + ' option:selected').val() == 1)) return false;
	var nouveau = new Option($('#' + src + ' option:selected').text(), $('#' + src + ' option:selected').val());
	$('#' + src + " option[value='" + $('#' + src + ' option:selected').val() + "']").remove()
	$('#' + dest ).append(nouveau)
	$('#' + dest ).removeAttr("selected");
}
function VerifMdp(wwwroot, mdp) { 
	if (mdp != "") {
		document.getElementById('imgmdp1').src = wwwroot + '/img/loader.gif';
		envoieRequete(wwwroot + '/modules/users/ajax/password.php?p=' + mdp,'verifmdp1');
	}
}
function VerifMdp2(wwwroot, mdp2) { 
	var mdp1 = document.getElementById('pass1').value;
	if (mdp2 != "") {
		document.getElementById('imgmdp2').src = wwwroot + '/img/loader.gif';
		envoieRequete(wwwroot + '/modules/users/ajax/password.php?p=' + mdp1 + '&m=' + mdp2,'verifmdp2');
	}
}
function Page(limite) {
	for(i=0;i<document.form.page.length;++i)
	if(document.form.page.options[i].value == limite)
	document.form.page.options[i].selected = true;
	document.form.submit();
}
function couleur(obj) {
    obj.style.backgroundColor = "#FFFFFF";
}
function VerifPseudo(wwwroot, pseudo) {
	if (pseudo != "") {
		document.getElementById('imgpseudo').src = wwwroot + '/img/loader.gif';
		envoieRequete(wwwroot + '/modules/users/ajax/pseudo.php?p=' + pseudo,'veriflog');
	}
}
function AlertMsg() {
	$( "#dialog-message" ).dialog({
		modal: true,
		width: 'auto',
		resizable: false,
		buttons: {
			Ok: function() {
				$( this ).dialog( "close" );
			}
		}
	});
};
function virgule(texte) {
	if (event.keyCode == 13) return false;
	else {
		while(texte.indexOf(',')>-1){
			texte=texte.replace(",",".");
		}
		return texte
	}
}
function CheckAllFichier(type) {
	var nb_fichier_max = $('#value_fichier_max').val();
	if (type == 1) {
		for (i=0; i<=nb_fichier_max; i++) {
			document.getElementById('fichier' + i).checked = true;
		}
		document.getElementById('checknone').checked = false;
	} else if (type == 2) {
		for (i=0; i<=nb_fichier_max; i++) {
			document.getElementById('fichier' + i).checked = false;
		}
		document.getElementById('checkall').checked = false;
		document.getElementById('checknone').checked = false;
	} else if (type == 3) {
		document.getElementById('checkall').checked = false;
	}
}
function SuppUploads() {
	var nb_fichier_max = $('#value_fichier_max').val();
	var verif = 0;
	var dialog_div = '';
	for (i=0; i<=nb_fichier_max; i++) {
		if (document.getElementById('fichier' + i).checked == true) verif++;
	}
	if (verif == 0) {
		dialog_div = '<div id="dialog-confirm-supp" title="Confirmation de suppression" style="display:none;">';
		dialog_div = dialog_div + '<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>';
		dialog_div = dialog_div + 'Vous devez s&eacute;lectionner au moins 1 fichier.';
		dialog_div = dialog_div + '</p></div>';
		if ($('#dialog-confirm-supp')) $('#dialog-confirm-supp').remove();
		$('body').append(dialog_div);
		$( "#dialog-confirm-supp" ).dialog({
			resizable: false,
			modal: true,
			buttons: {
				"OK": function() {
					$( this ).dialog( "close" );
				}
			}
		});
	} else {
		dialog_div = '<div id="dialog-confirm-supp" title="Confirmation de suppression" style="display:none;">';
		dialog_div = dialog_div + '<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>';
		dialog_div = dialog_div + '&Ecirc;tes-vous s&ucirc;r de vouloir supprimer ce(s) fichier(s) ?';
		dialog_div = dialog_div + '</p></div>';
		if ($('#dialog-confirm-supp')) $('#dialog-confirm-supp').remove();
		$('body').append(dialog_div);
		$( "#dialog-confirm-supp" ).dialog({
			resizable: false,
			modal: true,
			buttons: {
				"Supprimer": function() {
					$('#form_upload').submit();
				},
				"Annuler": function() {
					$( this ).dialog( "close" );
				}
			}
		});
	}
}
function ShowAlerts(type) {
	if (type == 'month') {
		$('table.alertes-content tr.infos-alerte').hide();
		$('table.alertes-content tr.month').show();
		$('table.titre-alertes-content td#choice-1').removeClass('selected');
		$('table.titre-alertes-content td#choice-2').addClass('selected');
		$('#type_view').val('month');
		$('table.alertes-content tr td.btn_actions').find('a').each(function(){
			$(this).attr('href',$(this).attr('href').replace('type_view=week','type_view=month'));
		});
	} else if (type == 'week') {
		$('table.alertes-content tr.infos-alerte').show();
		$('table.alertes-content tr.month').hide();
		$('table.titre-alertes-content td#choice-2').removeClass('selected');
		$('table.titre-alertes-content td#choice-1').addClass('selected');
		$('#type_view').val('week');
		$('table.alertes-content tr td.btn_actions').find('a').each(function(){
			$(this).attr('href',$(this).attr('href').replace('type_view=month','type_view=week'));
		});
	}
}
function SelectRegimeTVA() {
	var selected_regime = $('#regime_tva_entreprise option:selected').val();
	if (selected_regime > 1) {
		$('.tva-no').hide();
		$('.tva-yes').show();
	} else {
		$('.tva-no').show();
		$('.tva-yes').hide();
	}
}
function NewEmailValidation(sid) {
	var input = '<input type="hidden" name="sid_e" value="' + sid + '" />';
	input += '<input type="hidden" name="NewEmailValidation" value="1" />';
	$('#form_connect').append(input).submit();
}
function VerificationFirstConnexion() {
	var msg = '';
	var msg2 = '';
	if ($('#cgv').attr('checked') != 'checked') msg2 = '<b>Vous devez accepter les conditions g&eacute;n&eacute;rales pour continuer.</b><br /><br />';
	if ($('#nom_entreprise').val() == '') msg += '<li>Nom de votre soci&eacute;t&eacute;</li>';
	if ($('#adresse_entreprise').val() == '') msg += '<li>L\'adresse de votre soci&eacute;t&eacute;</li>'
	if ($('#cp_entreprise').val() == '') msg += '<li>Le code postal</li>';
	if ($('#ville_entreprise').val() == '') msg += '<li>Le nom de la ville</li>';
	if ($('#prenom_contact').val() == '') msg += '<li>Le pr&eacute;nom du contact</li>';
	if ($('#nom_contact').val() == '') msg += '<li>Le nom du contact</li>';
	if ((msg == '') && (msg2 == '')) $('#form_societe').submit();
	else {
		if (msg != '') msg = 'Vous devez renseigner les &eacute;l&eacute;ments suivants :<ul class="show-list">' + msg + '</ul>';
		$('#ui-dialog-title-dialog-message').html('Erreurs dans le formulaire');
		$('#dialog-message-content').html(msg2 + msg);
		AlertMsg();
	}
}
function GoToPremiersPasForm(page) {
	var data_form = $('#form_premiers_pas').serialize();
	$('#shadowbox-content').html('<img src="/img/loader2.gif" alt="" style="margin: 30px 0 30px 340px;" />');
	$.ajax({type:"POST", data: data_form, url:"/include/premiers_pas/page" + page + ".php",
		success: function(data){
			$('#shadowbox-content').html(data);
			if (jQuery(window).height() < $('#shadowbox-content').outerHeight()) {
				$('#shadowbox-content').css('position','absolute');
				$('html,body').animate({scrollTop: 0},0);
			} else $('#shadowbox-content').css('position','fixed').css('top',(parseInt(jQuery(window).height() - $('#shadowbox-content').height()) / 4) + 'px');
		}
	});
}
function GoToPremiersPasiFrame(page) {
	$('#form_premiers_pas').submit();
	$('#form_target').load(function(){
		GoToPremiersPas(page);
	});
}
function GoToPremiersPas(page) {
	$('#shadowbox-content').html('<img src="/img/loader2.gif" alt="" style="margin: 30px 0 30px 340px;" />');
	$('#shadowbox-content').load('/include/premiers_pas/page' + page + '.php',
		function(responseText, textStatus, XMLHttpRequest){
			if (textStatus == "success") {
				if (jQuery(window).height() < $('#shadowbox-content').outerHeight()) {
					$('#shadowbox-content').css('position','absolute');
					$('html,body').animate({scrollTop: 0},0);
				} else $('#shadowbox-content').css('position','fixed').css('top',(parseInt(jQuery(window).height() - $('#shadowbox-content').height()) / 4) + 'px');
			}
		}
	);
}
function LoadHelpContent(wwwroot,page) {
	$('.help-btn a').click(function(){
		$('#help-content').show().html('<img src="' + wwwroot + '/img/loader2.gif" alt="" class="loader" />').load(
			wwwroot + '/modules/aide/aide_rapide.php',
			{'page_help': page},
			function(responseText, textStatus, XMLHttpRequest){
				if (textStatus == "success") {
					$('#help-content .btn-close').click(function(){ $('#help-content').hide(); });
					$('#help-content .left-content').height($('#help-content').height() - 1);
					$('#help-content .right-content ul').height($('#help-content').height() - 331);
					$(document).keyup(function(e) {if (e.keyCode == 27) { $('#help-content').hide(); }});
				}
			}
		);
	});
}
function HelpFormSubmit(wwwroot) {
	if ($('#msg_help_form').val() == '') alert('Vous devez renseigner un message.');
	else {
		var data_post = $('#help_form').serialize();
		$.ajax({
			type: "POST",
			url: wwwroot + "/modules/aide/ajax/submit_form.php",
			data: data_post,
			success: function(data){ $('#help_contact_form').html(data); }
		});
	}
}