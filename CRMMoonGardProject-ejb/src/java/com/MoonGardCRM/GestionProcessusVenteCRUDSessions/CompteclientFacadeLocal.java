/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionProcessusVenteCRUDSessions;

import com.MoonGardCRM.GestionProcessusVenteEntities.Compteclient;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Jerbi
 */
@Local
public interface CompteclientFacadeLocal {

    void create(Compteclient compteclient);

    Compteclient edit(Compteclient compteclient);

    void remove(Compteclient compteclient);

    Compteclient find(Object id);

    List<Compteclient> findAll();

    List<Compteclient> findRange(int[] range);

    int count();
    
}
