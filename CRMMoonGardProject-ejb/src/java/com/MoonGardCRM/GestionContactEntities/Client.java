/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionContactEntities;

import com.MoonGardCRM.GestionProcessusVenteEntities.Commandevente;
import com.MoonGardCRM.GestionProcessusVenteEntities.Compteclient;
import com.MoonGardCRM.GestionProcessusVenteEntities.Devisvente;
import com.MoonGardCRM.GestionProcessusVenteEntities.Facturevente;
import com.MoonGardCRM.GestionProcessusVenteEntities.Livraisonvente;
import com.MoonGardCRM.GestionProcessusVenteEntities.Recouvrementclient;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jerbi
 */
@Entity
@Table(name = "client")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Client.findAll", query = "SELECT c FROM Client c"),
    @NamedQuery(name = "Client.findByIdcontact", query = "SELECT c FROM Client c WHERE c.idcontact = :idcontact"),
    @NamedQuery(name = "Client.findByCumulca", query = "SELECT c FROM Client c WHERE c.cumulca = :cumulca"),
    @NamedQuery(name = "Client.findByCumulcasolde", query = "SELECT c FROM Client c WHERE c.cumulcasolde = :cumulcasolde"),
    @NamedQuery(name = "Client.findByEntreprise", query = "SELECT c FROM Client c WHERE c.entreprise = :entreprise")})
public class Client implements Serializable {
    @OneToMany(mappedBy = "idcontact")
    private Collection<Livraisonvente> livraisonventeCollection;
    @OneToMany(mappedBy = "idcontact")
    private Collection<Devisvente> devisventeCollection;
    @OneToMany(mappedBy = "idcontact")
    private Collection<Compteclient> compteclientCollection;
    @OneToMany(mappedBy = "idcontact")
    private Collection<Facturevente> factureventeCollection;
    @OneToMany(mappedBy = "idcontact")
    private Collection<Commandevente> commandeventeCollection;
    @OneToMany(mappedBy = "idcontact")
    private Collection<Recouvrementclient> recouvrementclientCollection;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "idcontact")
    private Long idcontact;
    @Column(name = "cumulca")
    private BigInteger cumulca;
    @Column(name = "cumulcasolde")
    private BigInteger cumulcasolde;
    @Size(max = 254)
    @Column(name = "entreprise")
    private String entreprise;
    @JoinColumn(name = "idcontact", referencedColumnName = "idcontact", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Contact contact;

    public Client() {
    }

    public Client(Long idcontact) {
        this.idcontact = idcontact;
    }

    public Long getIdcontact() {
        return idcontact;
    }

    public void setIdcontact(Long idcontact) {
        this.idcontact = idcontact;
    }

    public BigInteger getCumulca() {
        return cumulca;
    }

    public void setCumulca(BigInteger cumulca) {
        this.cumulca = cumulca;
    }

    public BigInteger getCumulcasolde() {
        return cumulcasolde;
    }

    public void setCumulcasolde(BigInteger cumulcasolde) {
        this.cumulcasolde = cumulcasolde;
    }

    public String getEntreprise() {
        return entreprise;
    }

    public void setEntreprise(String entreprise) {
        this.entreprise = entreprise;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idcontact != null ? idcontact.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Client)) {
            return false;
        }
        Client other = (Client) object;
        if ((this.idcontact == null && other.idcontact != null) || (this.idcontact != null && !this.idcontact.equals(other.idcontact))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.MoonGardCRM.GestionContactEntities.Client[ idcontact=" + idcontact + " ]";
    }

    @XmlTransient
    public Collection<Livraisonvente> getLivraisonventeCollection() {
        return livraisonventeCollection;
    }

    public void setLivraisonventeCollection(Collection<Livraisonvente> livraisonventeCollection) {
        this.livraisonventeCollection = livraisonventeCollection;
    }

    @XmlTransient
    public Collection<Devisvente> getDevisventeCollection() {
        return devisventeCollection;
    }

    public void setDevisventeCollection(Collection<Devisvente> devisventeCollection) {
        this.devisventeCollection = devisventeCollection;
    }

    @XmlTransient
    public Collection<Compteclient> getCompteclientCollection() {
        return compteclientCollection;
    }

    public void setCompteclientCollection(Collection<Compteclient> compteclientCollection) {
        this.compteclientCollection = compteclientCollection;
    }

    @XmlTransient
    public Collection<Facturevente> getFactureventeCollection() {
        return factureventeCollection;
    }

    public void setFactureventeCollection(Collection<Facturevente> factureventeCollection) {
        this.factureventeCollection = factureventeCollection;
    }

    @XmlTransient
    public Collection<Commandevente> getCommandeventeCollection() {
        return commandeventeCollection;
    }

    public void setCommandeventeCollection(Collection<Commandevente> commandeventeCollection) {
        this.commandeventeCollection = commandeventeCollection;
    }

    @XmlTransient
    public Collection<Recouvrementclient> getRecouvrementclientCollection() {
        return recouvrementclientCollection;
    }

    public void setRecouvrementclientCollection(Collection<Recouvrementclient> recouvrementclientCollection) {
        this.recouvrementclientCollection = recouvrementclientCollection;
    }
    
}
