/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionProduitCRUDSessions;

import com.MoonGardCRM.GestionProduitEntities.Stockproduit;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Jerbi
 */
@Local
public interface StockproduitFacadeLocal {

    void create(Stockproduit stockproduit);

    Stockproduit edit(Stockproduit stockproduit);

    void remove(Stockproduit stockproduit);

    Stockproduit find(Object id);

    List<Stockproduit> findAll();

    List<Stockproduit> findRange(int[] range);

    int count();
    
}
