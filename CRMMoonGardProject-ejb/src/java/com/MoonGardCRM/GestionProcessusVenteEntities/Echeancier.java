/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionProcessusVenteEntities;

import com.MoonGardCRM.GestionRelanceClientEntities.Ficherelance;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jerbi
 */
@Entity
@Table(name = "echeancier")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Echeancier.findAll", query = "SELECT e FROM Echeancier e"),
    @NamedQuery(name = "Echeancier.findByRefecheancier", query = "SELECT e FROM Echeancier e WHERE e.refecheancier = :refecheancier"),
    @NamedQuery(name = "Echeancier.findByDateecheancier", query = "SELECT e FROM Echeancier e WHERE e.dateecheancier = :dateecheancier")})
public class Echeancier implements Serializable {
    @OneToMany(mappedBy = "refecheancier")
    private Collection<Ficherelance> ficherelanceCollection;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "refecheancier")
    private Long refecheancier;
    @Column(name = "dateecheancier")
    @Temporal(TemporalType.DATE)
    private Date dateecheancier;
    @JoinColumn(name = "refrecouvrementclient", referencedColumnName = "refrecouvrementclient")
    @ManyToOne
    private Recouvrementclient refrecouvrementclient;
    @OneToMany(mappedBy = "refecheancier")
    private Collection<Payementfacturevente> payementfactureventeCollection;

    public Echeancier() {
    }

    public Echeancier(Long refecheancier) {
        this.refecheancier = refecheancier;
    }

    public Long getRefecheancier() {
        return refecheancier;
    }

    public void setRefecheancier(Long refecheancier) {
        this.refecheancier = refecheancier;
    }

    public Date getDateecheancier() {
        return dateecheancier;
    }

    public void setDateecheancier(Date dateecheancier) {
        this.dateecheancier = dateecheancier;
    }

    public Recouvrementclient getRefrecouvrementclient() {
        return refrecouvrementclient;
    }

    public void setRefrecouvrementclient(Recouvrementclient refrecouvrementclient) {
        this.refrecouvrementclient = refrecouvrementclient;
    }

    @XmlTransient
    public Collection<Payementfacturevente> getPayementfactureventeCollection() {
        return payementfactureventeCollection;
    }

    public void setPayementfactureventeCollection(Collection<Payementfacturevente> payementfactureventeCollection) {
        this.payementfactureventeCollection = payementfactureventeCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refecheancier != null ? refecheancier.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Echeancier)) {
            return false;
        }
        Echeancier other = (Echeancier) object;
        if ((this.refecheancier == null && other.refecheancier != null) || (this.refecheancier != null && !this.refecheancier.equals(other.refecheancier))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.MoonGardCRM.GestionProcessusVenteEntities.Echeancier[ refecheancier=" + refecheancier + " ]";
    }

    @XmlTransient
    public Collection<Ficherelance> getFicherelanceCollection() {
        return ficherelanceCollection;
    }

    public void setFicherelanceCollection(Collection<Ficherelance> ficherelanceCollection) {
        this.ficherelanceCollection = ficherelanceCollection;
    }
    
}
