/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionRelanceClientEntities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jerbi
 */
@Entity
@Table(name = "relanceclient")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Relanceclient.findAll", query = "SELECT r FROM Relanceclient r"),
    @NamedQuery(name = "Relanceclient.findByRefrelanceclient", query = "SELECT r FROM Relanceclient r WHERE r.refrelanceclient = :refrelanceclient"),
    @NamedQuery(name = "Relanceclient.findByDatecreation", query = "SELECT r FROM Relanceclient r WHERE r.datecreation = :datecreation")})
public class Relanceclient implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "refrelanceclient")
    private Long refrelanceclient;
    @Column(name = "datecreation")
    @Temporal(TemporalType.DATE)
    private Date datecreation;
    @OneToMany(mappedBy = "refrelanceclient")
    private Collection<Ficherelance> ficherelanceCollection;

    public Relanceclient() {
    }

    public Relanceclient(Long refrelanceclient) {
        this.refrelanceclient = refrelanceclient;
    }

    public Long getRefrelanceclient() {
        return refrelanceclient;
    }

    public void setRefrelanceclient(Long refrelanceclient) {
        this.refrelanceclient = refrelanceclient;
    }

    public Date getDatecreation() {
        return datecreation;
    }

    public void setDatecreation(Date datecreation) {
        this.datecreation = datecreation;
    }

    @XmlTransient
    public Collection<Ficherelance> getFicherelanceCollection() {
        return ficherelanceCollection;
    }

    public void setFicherelanceCollection(Collection<Ficherelance> ficherelanceCollection) {
        this.ficherelanceCollection = ficherelanceCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refrelanceclient != null ? refrelanceclient.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Relanceclient)) {
            return false;
        }
        Relanceclient other = (Relanceclient) object;
        if ((this.refrelanceclient == null && other.refrelanceclient != null) || (this.refrelanceclient != null && !this.refrelanceclient.equals(other.refrelanceclient))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.MoonGardCRM.GestionRelanceClientEntities.Relanceclient[ refrelanceclient=" + refrelanceclient + " ]";
    }
    
}
