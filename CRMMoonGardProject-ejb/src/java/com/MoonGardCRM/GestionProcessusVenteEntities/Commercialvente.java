/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionProcessusVenteEntities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jerbi
 */
@Entity
@Table(name = "commercialvente")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Commercialvente.findAll", query = "SELECT c FROM Commercialvente c"),
    @NamedQuery(name = "Commercialvente.findByRefcommercialvente", query = "SELECT c FROM Commercialvente c WHERE c.refcommercialvente = :refcommercialvente"),
    @NamedQuery(name = "Commercialvente.findByNom", query = "SELECT c FROM Commercialvente c WHERE c.nom = :nom"),
    @NamedQuery(name = "Commercialvente.findByPrenom", query = "SELECT c FROM Commercialvente c WHERE c.prenom = :prenom")})
public class Commercialvente implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "refcommercialvente")
    private Long refcommercialvente;
    @Size(max = 254)
    @Column(name = "nom")
    private String nom;
    @Size(max = 254)
    @Column(name = "prenom")
    private String prenom;
    @OneToMany(mappedBy = "refcommercialvente")
    private Collection<Devisvente> devisventeCollection;
    @OneToMany(mappedBy = "refcommercialvente")
    private Collection<Commandevente> commandeventeCollection;

    public Commercialvente() {
    }

    public Commercialvente(Long refcommercialvente) {
        this.refcommercialvente = refcommercialvente;
    }

    public Long getRefcommercialvente() {
        return refcommercialvente;
    }

    public void setRefcommercialvente(Long refcommercialvente) {
        this.refcommercialvente = refcommercialvente;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    @XmlTransient
    public Collection<Devisvente> getDevisventeCollection() {
        return devisventeCollection;
    }

    public void setDevisventeCollection(Collection<Devisvente> devisventeCollection) {
        this.devisventeCollection = devisventeCollection;
    }

    @XmlTransient
    public Collection<Commandevente> getCommandeventeCollection() {
        return commandeventeCollection;
    }

    public void setCommandeventeCollection(Collection<Commandevente> commandeventeCollection) {
        this.commandeventeCollection = commandeventeCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refcommercialvente != null ? refcommercialvente.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Commercialvente)) {
            return false;
        }
        Commercialvente other = (Commercialvente) object;
        if ((this.refcommercialvente == null && other.refcommercialvente != null) || (this.refcommercialvente != null && !this.refcommercialvente.equals(other.refcommercialvente))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.MoonGardCRM.GestionProcessusVenteEntities.Commercialvente[ refcommercialvente=" + refcommercialvente + " ]";
    }
    
}
