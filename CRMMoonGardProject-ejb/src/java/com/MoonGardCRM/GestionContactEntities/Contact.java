/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionContactEntities;

import com.MoonGardCRM.GestionMarketingEntities.Newslettercontact;
import com.MoonGardCRM.GestionRelanceClientEntities.Ficherelance;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jerbi
 */
@Entity
@Table(name = "contact")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Contact.findAll", query = "SELECT c FROM Contact c"),
    @NamedQuery(name = "Contact.findByIdcontact", query = "SELECT c FROM Contact c WHERE c.idcontact = :idcontact"),
    @NamedQuery(name = "Contact.findByCin", query = "SELECT c FROM Contact c WHERE c.cin = :cin"),
    @NamedQuery(name = "Contact.findByCivilite", query = "SELECT c FROM Contact c WHERE c.civilite = :civilite"),
    @NamedQuery(name = "Contact.findByNom", query = "SELECT c FROM Contact c WHERE c.nom = :nom"),
    @NamedQuery(name = "Contact.findByPrenom", query = "SELECT c FROM Contact c WHERE c.prenom = :prenom"),
    @NamedQuery(name = "Contact.findByDatenaissance", query = "SELECT c FROM Contact c WHERE c.datenaissance = :datenaissance"),
    @NamedQuery(name = "Contact.findByEmail", query = "SELECT c FROM Contact c WHERE c.email = :email"),
    @NamedQuery(name = "Contact.findByGsm", query = "SELECT c FROM Contact c WHERE c.gsm = :gsm"),
    @NamedQuery(name = "Contact.findByTelephone", query = "SELECT c FROM Contact c WHERE c.telephone = :telephone"),
    @NamedQuery(name = "Contact.findByFax", query = "SELECT c FROM Contact c WHERE c.fax = :fax"),
    @NamedQuery(name = "Contact.findBySiteweb", query = "SELECT c FROM Contact c WHERE c.siteweb = :siteweb"),
    @NamedQuery(name = "Contact.findByDomaineactivitee", query = "SELECT c FROM Contact c WHERE c.domaineactivitee = :domaineactivitee"),
    @NamedQuery(name = "Contact.findByDatecreation", query = "SELECT c FROM Contact c WHERE c.datecreation = :datecreation"),
    @NamedQuery(name = "Contact.findByDatederniercontact", query = "SELECT c FROM Contact c WHERE c.datederniercontact = :datederniercontact"),
    @NamedQuery(name = "Contact.findByDatereactivationarchive", query = "SELECT c FROM Contact c WHERE c.datereactivationarchive = :datereactivationarchive"),
    @NamedQuery(name = "Contact.findByObservation", query = "SELECT c FROM Contact c WHERE c.observation = :observation"),
    @NamedQuery(name = "Contact.findByNewsletter", query = "SELECT c FROM Contact c WHERE c.newsletter = :newsletter"),
    @NamedQuery(name = "Contact.findByFactureimpayee", query = "SELECT c FROM Contact c WHERE c.factureimpayee = :factureimpayee")})
public class Contact implements Serializable {
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "contact")
    private Collection<Newslettercontact> newslettercontactCollection;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "contact")
    private Contactarchivee contactarchivee;
    @OneToMany(mappedBy = "idcontact")
    private Collection<Ficherelance> ficherelanceCollection;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "idcontact")
    private Long idcontact;
    @Size(max = 254)
    @Column(name = "cin")
    private String cin;
    @Size(max = 254)
    @Column(name = "civilite")
    private String civilite;
    @Size(max = 254)
    @Column(name = "nom")
    private String nom;
    @Size(max = 254)
    @Column(name = "prenom")
    private String prenom;
    @Column(name = "datenaissance")
    @Temporal(TemporalType.DATE)
    private Date datenaissance;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 254)
    @Column(name = "email")
    private String email;
    @Size(max = 254)
    @Column(name = "gsm")
    private String gsm;
    @Size(max = 254)
    @Column(name = "telephone")
    private String telephone;
    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Size(max = 254)
    @Column(name = "fax")
    private String fax;
    @Column(name = "siteweb")
    private Integer siteweb;
    @Column(name = "domaineactivitee")
    private Integer domaineactivitee;
    @Column(name = "datecreation")
    @Temporal(TemporalType.DATE)
    private Date datecreation;
    @Column(name = "datederniercontact")
    @Temporal(TemporalType.DATE)
    private Date datederniercontact;
    @Column(name = "datereactivationarchive")
    @Temporal(TemporalType.DATE)
    private Date datereactivationarchive;
    @Size(max = 254)
    @Column(name = "observation")
    private String observation;
    @Column(name = "newsletter")
    private Boolean newsletter;
    @Column(name = "factureimpayee")
    private Boolean factureimpayee;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "contact")
    private Client client;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "contact")
    private Fournisseur fournisseur;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "contact")
    private Prospect prospect;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "contact")
    private Collection<Adressecontact> adressecontactCollection;

    public Contact() {
    }

    public Contact(Long idcontact) {
        this.idcontact = idcontact;
    }

    public Long getIdcontact() {
        return idcontact;
    }

    public void setIdcontact(Long idcontact) {
        this.idcontact = idcontact;
    }

    public String getCin() {
        return cin;
    }

    public void setCin(String cin) {
        this.cin = cin;
    }

    public String getCivilite() {
        return civilite;
    }

    public void setCivilite(String civilite) {
        this.civilite = civilite;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Date getDatenaissance() {
        return datenaissance;
    }

    public void setDatenaissance(Date datenaissance) {
        this.datenaissance = datenaissance;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGsm() {
        return gsm;
    }

    public void setGsm(String gsm) {
        this.gsm = gsm;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public Integer getSiteweb() {
        return siteweb;
    }

    public void setSiteweb(Integer siteweb) {
        this.siteweb = siteweb;
    }

    public Integer getDomaineactivitee() {
        return domaineactivitee;
    }

    public void setDomaineactivitee(Integer domaineactivitee) {
        this.domaineactivitee = domaineactivitee;
    }

    public Date getDatecreation() {
        return datecreation;
    }

    public void setDatecreation(Date datecreation) {
        this.datecreation = datecreation;
    }

    public Date getDatederniercontact() {
        return datederniercontact;
    }

    public void setDatederniercontact(Date datederniercontact) {
        this.datederniercontact = datederniercontact;
    }

    public Date getDatereactivationarchive() {
        return datereactivationarchive;
    }

    public void setDatereactivationarchive(Date datereactivationarchive) {
        this.datereactivationarchive = datereactivationarchive;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public Boolean getNewsletter() {
        return newsletter;
    }

    public void setNewsletter(Boolean newsletter) {
        this.newsletter = newsletter;
    }

    public Boolean getFactureimpayee() {
        return factureimpayee;
    }

    public void setFactureimpayee(Boolean factureimpayee) {
        this.factureimpayee = factureimpayee;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Fournisseur getFournisseur() {
        return fournisseur;
    }

    public void setFournisseur(Fournisseur fournisseur) {
        this.fournisseur = fournisseur;
    }

    public Prospect getProspect() {
        return prospect;
    }

    public void setProspect(Prospect prospect) {
        this.prospect = prospect;
    }

    @XmlTransient
    public Collection<Adressecontact> getAdressecontactCollection() {
        return adressecontactCollection;
    }

    public void setAdressecontactCollection(Collection<Adressecontact> adressecontactCollection) {
        this.adressecontactCollection = adressecontactCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idcontact != null ? idcontact.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Contact)) {
            return false;
        }
        Contact other = (Contact) object;
        if ((this.idcontact == null && other.idcontact != null) || (this.idcontact != null && !this.idcontact.equals(other.idcontact))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.MoonGardCRM.GestionContactEntities.Contact[ idcontact=" + idcontact + " ]";
    }

    @XmlTransient
    public Collection<Ficherelance> getFicherelanceCollection() {
        return ficherelanceCollection;
    }

    public void setFicherelanceCollection(Collection<Ficherelance> ficherelanceCollection) {
        this.ficherelanceCollection = ficherelanceCollection;
    }

    public Contactarchivee getContactarchivee() {
        return contactarchivee;
    }

    public void setContactarchivee(Contactarchivee contactarchivee) {
        this.contactarchivee = contactarchivee;
    }

    @XmlTransient
    public Collection<Newslettercontact> getNewslettercontactCollection() {
        return newslettercontactCollection;
    }

    public void setNewslettercontactCollection(Collection<Newslettercontact> newslettercontactCollection) {
        this.newslettercontactCollection = newslettercontactCollection;
    }
    
}
