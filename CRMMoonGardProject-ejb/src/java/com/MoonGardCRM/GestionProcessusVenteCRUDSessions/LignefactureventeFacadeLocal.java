/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionProcessusVenteCRUDSessions;

import com.MoonGardCRM.GestionProcessusVenteEntities.Lignefacturevente;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Jerbi
 */
@Local
public interface LignefactureventeFacadeLocal {

    void create(Lignefacturevente lignefacturevente);

    Lignefacturevente edit(Lignefacturevente lignefacturevente);

    void remove(Lignefacturevente lignefacturevente);

    Lignefacturevente find(Object id);

    List<Lignefacturevente> findAll();

    List<Lignefacturevente> findRange(int[] range);

    int count();
    
}
