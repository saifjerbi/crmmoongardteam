/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionProduitCRUDSessions;

import com.MoonGardCRM.GestionProduitEntities.Catalogueproduit;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Jerbi
 */
@Local
public interface CatalogueproduitFacadeLocal {

    void create(Catalogueproduit catalogueproduit);

    Catalogueproduit edit(Catalogueproduit catalogueproduit);

    void remove(Catalogueproduit catalogueproduit);

    Catalogueproduit find(Object id);

    List<Catalogueproduit> findAll();

    List<Catalogueproduit> findRange(int[] range);

    int count();
    
}
