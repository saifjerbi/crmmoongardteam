/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionProcessusVenteCRUDSessions;

import com.MoonGardCRM.GestionProcessusVenteEntities.Lignelivraisonvente;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Jerbi
 */
@Local
public interface LignelivraisonventeFacadeLocal {

    void create(Lignelivraisonvente lignelivraisonvente);

    Lignelivraisonvente edit(Lignelivraisonvente lignelivraisonvente);

    void remove(Lignelivraisonvente lignelivraisonvente);

    Lignelivraisonvente find(Object id);

    List<Lignelivraisonvente> findAll();

    List<Lignelivraisonvente> findRange(int[] range);

    int count();
    
}
