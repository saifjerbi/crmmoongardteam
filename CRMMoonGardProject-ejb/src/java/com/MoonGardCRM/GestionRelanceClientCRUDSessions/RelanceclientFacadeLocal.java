/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionRelanceClientCRUDSessions;

import com.MoonGardCRM.GestionRelanceClientEntities.Relanceclient;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Jerbi
 */
@Local
public interface RelanceclientFacadeLocal {

    void create(Relanceclient relanceclient);

    Relanceclient edit(Relanceclient relanceclient);

    void remove(Relanceclient relanceclient);

    Relanceclient find(Object id);

    List<Relanceclient> findAll();

    List<Relanceclient> findRange(int[] range);

    int count();
    
}
