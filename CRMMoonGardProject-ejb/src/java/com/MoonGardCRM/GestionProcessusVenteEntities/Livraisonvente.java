/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionProcessusVenteEntities;

import com.MoonGardCRM.GestionContactEntities.Client;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jerbi
 */
@Entity
@Table(name = "livraisonvente")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Livraisonvente.findAll", query = "SELECT l FROM Livraisonvente l"),
    @NamedQuery(name = "Livraisonvente.findByNumerobl", query = "SELECT l FROM Livraisonvente l WHERE l.numerobl = :numerobl"),
    @NamedQuery(name = "Livraisonvente.findByDatecreation", query = "SELECT l FROM Livraisonvente l WHERE l.datecreation = :datecreation"),
    @NamedQuery(name = "Livraisonvente.findByTotalttc", query = "SELECT l FROM Livraisonvente l WHERE l.totalttc = :totalttc"),
    @NamedQuery(name = "Livraisonvente.findByTotalht", query = "SELECT l FROM Livraisonvente l WHERE l.totalht = :totalht"),
    @NamedQuery(name = "Livraisonvente.findByTotaltva", query = "SELECT l FROM Livraisonvente l WHERE l.totaltva = :totaltva"),
    @NamedQuery(name = "Livraisonvente.findByNetapayer", query = "SELECT l FROM Livraisonvente l WHERE l.netapayer = :netapayer"),
    @NamedQuery(name = "Livraisonvente.findByAcompte", query = "SELECT l FROM Livraisonvente l WHERE l.acompte = :acompte"),
    @NamedQuery(name = "Livraisonvente.findByResteapayer", query = "SELECT l FROM Livraisonvente l WHERE l.resteapayer = :resteapayer"),
    @NamedQuery(name = "Livraisonvente.findByMessageentete", query = "SELECT l FROM Livraisonvente l WHERE l.messageentete = :messageentete"),
    @NamedQuery(name = "Livraisonvente.findByNotebaspage", query = "SELECT l FROM Livraisonvente l WHERE l.notebaspage = :notebaspage"),
    @NamedQuery(name = "Livraisonvente.findByModelivraison", query = "SELECT l FROM Livraisonvente l WHERE l.modelivraison = :modelivraison"),
    @NamedQuery(name = "Livraisonvente.findByFraislivraison", query = "SELECT l FROM Livraisonvente l WHERE l.fraislivraison = :fraislivraison"),
    @NamedQuery(name = "Livraisonvente.findByEtat", query = "SELECT l FROM Livraisonvente l WHERE l.etat = :etat")})
public class Livraisonvente implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "numerobl")
    private Long numerobl;
    @Column(name = "datecreation")
    @Temporal(TemporalType.DATE)
    private Date datecreation;
    @Column(name = "totalttc")
    private Integer totalttc;
    @Column(name = "totalht")
    private Integer totalht;
    @Column(name = "totaltva")
    private Integer totaltva;
    @Column(name = "netapayer")
    private Integer netapayer;
    @Column(name = "acompte")
    private BigInteger acompte;
    @Column(name = "resteapayer")
    private Integer resteapayer;
    @Size(max = 254)
    @Column(name = "messageentete")
    private String messageentete;
    @Size(max = 254)
    @Column(name = "notebaspage")
    private String notebaspage;
    @Size(max = 254)
    @Column(name = "modelivraison")
    private String modelivraison;
    @Column(name = "fraislivraison")
    private Integer fraislivraison;
    @Size(max = 254)
    @Column(name = "etat")
    private String etat;
    @JoinColumn(name = "refvehiculelivraison", referencedColumnName = "refvehiculelivraison")
    @ManyToOne
    private Vehiculelivraison refvehiculelivraison;
    @JoinColumn(name = "numerofact", referencedColumnName = "numerofact")
    @ManyToOne
    private Facturevente numerofact;
    @JoinColumn(name = "numerocom", referencedColumnName = "numerocom")
    @ManyToOne
    private Commandevente numerocom;
    @JoinColumn(name = "idcontact", referencedColumnName = "idcontact")
    @ManyToOne
    private Client idcontact;
    @OneToMany(mappedBy = "numerobl")
    private Collection<Lignelivraisonvente> lignelivraisonventeCollection;

    public Livraisonvente() {
    }

    public Livraisonvente(Long numerobl) {
        this.numerobl = numerobl;
    }

    public Long getNumerobl() {
        return numerobl;
    }

    public void setNumerobl(Long numerobl) {
        this.numerobl = numerobl;
    }

    public Date getDatecreation() {
        return datecreation;
    }

    public void setDatecreation(Date datecreation) {
        this.datecreation = datecreation;
    }

    public Integer getTotalttc() {
        return totalttc;
    }

    public void setTotalttc(Integer totalttc) {
        this.totalttc = totalttc;
    }

    public Integer getTotalht() {
        return totalht;
    }

    public void setTotalht(Integer totalht) {
        this.totalht = totalht;
    }

    public Integer getTotaltva() {
        return totaltva;
    }

    public void setTotaltva(Integer totaltva) {
        this.totaltva = totaltva;
    }

    public Integer getNetapayer() {
        return netapayer;
    }

    public void setNetapayer(Integer netapayer) {
        this.netapayer = netapayer;
    }

    public BigInteger getAcompte() {
        return acompte;
    }

    public void setAcompte(BigInteger acompte) {
        this.acompte = acompte;
    }

    public Integer getResteapayer() {
        return resteapayer;
    }

    public void setResteapayer(Integer resteapayer) {
        this.resteapayer = resteapayer;
    }

    public String getMessageentete() {
        return messageentete;
    }

    public void setMessageentete(String messageentete) {
        this.messageentete = messageentete;
    }

    public String getNotebaspage() {
        return notebaspage;
    }

    public void setNotebaspage(String notebaspage) {
        this.notebaspage = notebaspage;
    }

    public String getModelivraison() {
        return modelivraison;
    }

    public void setModelivraison(String modelivraison) {
        this.modelivraison = modelivraison;
    }

    public Integer getFraislivraison() {
        return fraislivraison;
    }

    public void setFraislivraison(Integer fraislivraison) {
        this.fraislivraison = fraislivraison;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public Vehiculelivraison getRefvehiculelivraison() {
        return refvehiculelivraison;
    }

    public void setRefvehiculelivraison(Vehiculelivraison refvehiculelivraison) {
        this.refvehiculelivraison = refvehiculelivraison;
    }

    public Facturevente getNumerofact() {
        return numerofact;
    }

    public void setNumerofact(Facturevente numerofact) {
        this.numerofact = numerofact;
    }

    public Commandevente getNumerocom() {
        return numerocom;
    }

    public void setNumerocom(Commandevente numerocom) {
        this.numerocom = numerocom;
    }

    public Client getIdcontact() {
        return idcontact;
    }

    public void setIdcontact(Client idcontact) {
        this.idcontact = idcontact;
    }

    @XmlTransient
    public Collection<Lignelivraisonvente> getLignelivraisonventeCollection() {
        return lignelivraisonventeCollection;
    }

    public void setLignelivraisonventeCollection(Collection<Lignelivraisonvente> lignelivraisonventeCollection) {
        this.lignelivraisonventeCollection = lignelivraisonventeCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (numerobl != null ? numerobl.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Livraisonvente)) {
            return false;
        }
        Livraisonvente other = (Livraisonvente) object;
        if ((this.numerobl == null && other.numerobl != null) || (this.numerobl != null && !this.numerobl.equals(other.numerobl))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.MoonGardCRM.GestionProcessusVenteEntities.Livraisonvente[ numerobl=" + numerobl + " ]";
    }
    
}
