/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionProcessusVenteCRUDSessions;

import com.MoonGardCRM.GestionProcessusVenteEntities.Chauffeur;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Jerbi
 */
@Local
public interface ChauffeurFacadeLocal {

    void create(Chauffeur chauffeur);

    Chauffeur edit(Chauffeur chauffeur);

    void remove(Chauffeur chauffeur);

    Chauffeur find(Object id);

    List<Chauffeur> findAll();

    List<Chauffeur> findRange(int[] range);

    int count();
    
}
