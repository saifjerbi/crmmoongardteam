/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionProduitEntities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jerbi
 */
@Entity
@Table(name = "catalogueproduit")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Catalogueproduit.findAll", query = "SELECT c FROM Catalogueproduit c"),
    @NamedQuery(name = "Catalogueproduit.findByRefcatalogueproduit", query = "SELECT c FROM Catalogueproduit c WHERE c.refcatalogueproduit = :refcatalogueproduit"),
    @NamedQuery(name = "Catalogueproduit.findByTypecatalogue", query = "SELECT c FROM Catalogueproduit c WHERE c.typecatalogue = :typecatalogue"),
    @NamedQuery(name = "Catalogueproduit.findByDatecreation", query = "SELECT c FROM Catalogueproduit c WHERE c.datecreation = :datecreation"),
    @NamedQuery(name = "Catalogueproduit.findByDatemodification", query = "SELECT c FROM Catalogueproduit c WHERE c.datemodification = :datemodification"),
    @NamedQuery(name = "Catalogueproduit.findByEtat", query = "SELECT c FROM Catalogueproduit c WHERE c.etat = :etat")})
public class Catalogueproduit implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "refcatalogueproduit")
    private Long refcatalogueproduit;
    @Size(max = 254)
    @Column(name = "typecatalogue")
    private String typecatalogue;
    @Column(name = "datecreation")
    @Temporal(TemporalType.DATE)
    private Date datecreation;
    @Column(name = "datemodification")
    @Temporal(TemporalType.DATE)
    private Date datemodification;
    @Size(max = 254)
    @Column(name = "etat")
    private String etat;
    @JoinTable(name = "agrnewsletter_catalogueproduit", joinColumns = {
        @JoinColumn(name = "refcatalogueproduit", referencedColumnName = "refcatalogueproduit")}, inverseJoinColumns = {
        @JoinColumn(name = "refnewsletter", referencedColumnName = "refnewsletter")})
    @ManyToMany
    private Collection<Newsletter> newsletterCollection;
    @OneToMany(mappedBy = "refcatalogueproduit")
    private Collection<Produit> produitCollection;

    public Catalogueproduit() {
    }

    public Catalogueproduit(Long refcatalogueproduit) {
        this.refcatalogueproduit = refcatalogueproduit;
    }

    public Long getRefcatalogueproduit() {
        return refcatalogueproduit;
    }

    public void setRefcatalogueproduit(Long refcatalogueproduit) {
        this.refcatalogueproduit = refcatalogueproduit;
    }

    public String getTypecatalogue() {
        return typecatalogue;
    }

    public void setTypecatalogue(String typecatalogue) {
        this.typecatalogue = typecatalogue;
    }

    public Date getDatecreation() {
        return datecreation;
    }

    public void setDatecreation(Date datecreation) {
        this.datecreation = datecreation;
    }

    public Date getDatemodification() {
        return datemodification;
    }

    public void setDatemodification(Date datemodification) {
        this.datemodification = datemodification;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    @XmlTransient
    public Collection<Newsletter> getNewsletterCollection() {
        return newsletterCollection;
    }

    public void setNewsletterCollection(Collection<Newsletter> newsletterCollection) {
        this.newsletterCollection = newsletterCollection;
    }

    @XmlTransient
    public Collection<Produit> getProduitCollection() {
        return produitCollection;
    }

    public void setProduitCollection(Collection<Produit> produitCollection) {
        this.produitCollection = produitCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refcatalogueproduit != null ? refcatalogueproduit.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Catalogueproduit)) {
            return false;
        }
        Catalogueproduit other = (Catalogueproduit) object;
        if ((this.refcatalogueproduit == null && other.refcatalogueproduit != null) || (this.refcatalogueproduit != null && !this.refcatalogueproduit.equals(other.refcatalogueproduit))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.MoonGardCRM.GestionProduitEntities.Catalogueproduit[ refcatalogueproduit=" + refcatalogueproduit + " ]";
    }
    
}
