/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionProcessusVenteCRUDSessions;

import com.MoonGardCRM.GestionProcessusVenteEntities.Vehiculelivraison;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Jerbi
 */
@Local
public interface VehiculelivraisonFacadeLocal {

    void create(Vehiculelivraison vehiculelivraison);

    Vehiculelivraison edit(Vehiculelivraison vehiculelivraison);

    void remove(Vehiculelivraison vehiculelivraison);

    Vehiculelivraison find(Object id);

    List<Vehiculelivraison> findAll();

    List<Vehiculelivraison> findRange(int[] range);

    int count();
    
}
