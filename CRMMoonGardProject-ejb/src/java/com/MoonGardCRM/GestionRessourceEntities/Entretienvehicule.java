/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionRessourceEntities;

import com.MoonGardCRM.GestionProcessusVenteEntities.Vehiculelivraison;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jerbi
 */
@Entity
@Table(name = "entretienvehicule")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Entretienvehicule.findAll", query = "SELECT e FROM Entretienvehicule e"),
    @NamedQuery(name = "Entretienvehicule.findByRefentretienvehicule", query = "SELECT e FROM Entretienvehicule e WHERE e.refentretienvehicule = :refentretienvehicule"),
    @NamedQuery(name = "Entretienvehicule.findByDateentretien", query = "SELECT e FROM Entretienvehicule e WHERE e.dateentretien = :dateentretien"),
    @NamedQuery(name = "Entretienvehicule.findByCoutentretien", query = "SELECT e FROM Entretienvehicule e WHERE e.coutentretien = :coutentretien"),
    @NamedQuery(name = "Entretienvehicule.findByNote", query = "SELECT e FROM Entretienvehicule e WHERE e.note = :note")})
public class Entretienvehicule implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "refentretienvehicule")
    private Long refentretienvehicule;
    @Column(name = "dateentretien")
    @Temporal(TemporalType.DATE)
    private Date dateentretien;
    @Column(name = "coutentretien")
    private BigInteger coutentretien;
    @Size(max = 254)
    @Column(name = "note")
    private String note;
    @JoinColumn(name = "refvehiculelivraison", referencedColumnName = "refvehiculelivraison")
    @ManyToOne
    private Vehiculelivraison refvehiculelivraison;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "entretienvehicule")
    private Vidange vidange;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "entretienvehicule")
    private Lavage lavage;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "entretienvehicule")
    private Rechangepiece rechangepiece;

    public Entretienvehicule() {
    }

    public Entretienvehicule(Long refentretienvehicule) {
        this.refentretienvehicule = refentretienvehicule;
    }

    public Long getRefentretienvehicule() {
        return refentretienvehicule;
    }

    public void setRefentretienvehicule(Long refentretienvehicule) {
        this.refentretienvehicule = refentretienvehicule;
    }

    public Date getDateentretien() {
        return dateentretien;
    }

    public void setDateentretien(Date dateentretien) {
        this.dateentretien = dateentretien;
    }

    public BigInteger getCoutentretien() {
        return coutentretien;
    }

    public void setCoutentretien(BigInteger coutentretien) {
        this.coutentretien = coutentretien;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Vehiculelivraison getRefvehiculelivraison() {
        return refvehiculelivraison;
    }

    public void setRefvehiculelivraison(Vehiculelivraison refvehiculelivraison) {
        this.refvehiculelivraison = refvehiculelivraison;
    }

    public Vidange getVidange() {
        return vidange;
    }

    public void setVidange(Vidange vidange) {
        this.vidange = vidange;
    }

    public Lavage getLavage() {
        return lavage;
    }

    public void setLavage(Lavage lavage) {
        this.lavage = lavage;
    }

    public Rechangepiece getRechangepiece() {
        return rechangepiece;
    }

    public void setRechangepiece(Rechangepiece rechangepiece) {
        this.rechangepiece = rechangepiece;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refentretienvehicule != null ? refentretienvehicule.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Entretienvehicule)) {
            return false;
        }
        Entretienvehicule other = (Entretienvehicule) object;
        if ((this.refentretienvehicule == null && other.refentretienvehicule != null) || (this.refentretienvehicule != null && !this.refentretienvehicule.equals(other.refentretienvehicule))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.MoonGardCRM.GestionRessourceEntities.Entretienvehicule[ refentretienvehicule=" + refentretienvehicule + " ]";
    }
    
}
