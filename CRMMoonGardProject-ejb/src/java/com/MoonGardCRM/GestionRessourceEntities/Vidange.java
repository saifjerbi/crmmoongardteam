/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionRessourceEntities;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jerbi
 */
@Entity
@Table(name = "vidange")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Vidange.findAll", query = "SELECT v FROM Vidange v"),
    @NamedQuery(name = "Vidange.findByRefentretienvehicule", query = "SELECT v FROM Vidange v WHERE v.refentretienvehicule = :refentretienvehicule"),
    @NamedQuery(name = "Vidange.findByDateprochainvidange", query = "SELECT v FROM Vidange v WHERE v.dateprochainvidange = :dateprochainvidange"),
    @NamedQuery(name = "Vidange.findByKilometrageavantvidange", query = "SELECT v FROM Vidange v WHERE v.kilometrageavantvidange = :kilometrageavantvidange"),
    @NamedQuery(name = "Vidange.findByHuileutilise", query = "SELECT v FROM Vidange v WHERE v.huileutilise = :huileutilise"),
    @NamedQuery(name = "Vidange.findByLieu", query = "SELECT v FROM Vidange v WHERE v.lieu = :lieu")})
public class Vidange implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "refentretienvehicule")
    private Long refentretienvehicule;
    @Column(name = "dateprochainvidange")
    @Temporal(TemporalType.DATE)
    private Date dateprochainvidange;
    @Column(name = "kilometrageavantvidange")
    private BigInteger kilometrageavantvidange;
    @Size(max = 254)
    @Column(name = "huileutilise")
    private String huileutilise;
    @Size(max = 254)
    @Column(name = "lieu")
    private String lieu;
    @JoinColumn(name = "refentretienvehicule", referencedColumnName = "refentretienvehicule", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Entretienvehicule entretienvehicule;

    public Vidange() {
    }

    public Vidange(Long refentretienvehicule) {
        this.refentretienvehicule = refentretienvehicule;
    }

    public Long getRefentretienvehicule() {
        return refentretienvehicule;
    }

    public void setRefentretienvehicule(Long refentretienvehicule) {
        this.refentretienvehicule = refentretienvehicule;
    }

    public Date getDateprochainvidange() {
        return dateprochainvidange;
    }

    public void setDateprochainvidange(Date dateprochainvidange) {
        this.dateprochainvidange = dateprochainvidange;
    }

    public BigInteger getKilometrageavantvidange() {
        return kilometrageavantvidange;
    }

    public void setKilometrageavantvidange(BigInteger kilometrageavantvidange) {
        this.kilometrageavantvidange = kilometrageavantvidange;
    }

    public String getHuileutilise() {
        return huileutilise;
    }

    public void setHuileutilise(String huileutilise) {
        this.huileutilise = huileutilise;
    }

    public String getLieu() {
        return lieu;
    }

    public void setLieu(String lieu) {
        this.lieu = lieu;
    }

    public Entretienvehicule getEntretienvehicule() {
        return entretienvehicule;
    }

    public void setEntretienvehicule(Entretienvehicule entretienvehicule) {
        this.entretienvehicule = entretienvehicule;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refentretienvehicule != null ? refentretienvehicule.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Vidange)) {
            return false;
        }
        Vidange other = (Vidange) object;
        if ((this.refentretienvehicule == null && other.refentretienvehicule != null) || (this.refentretienvehicule != null && !this.refentretienvehicule.equals(other.refentretienvehicule))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.MoonGardCRM.GestionRessourceEntities.Vidange[ refentretienvehicule=" + refentretienvehicule + " ]";
    }
    
}
