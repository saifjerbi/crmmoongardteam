/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionRessourceCRUDSession;

import com.MoonGardCRM.GestionRessourceEntities.Entretienvehicule;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Jerbi
 */
@Local
public interface EntretienvehiculeFacadeLocal {

    void create(Entretienvehicule entretienvehicule);

    Entretienvehicule edit(Entretienvehicule entretienvehicule);

    void remove(Entretienvehicule entretienvehicule);

    Entretienvehicule find(Object id);

    List<Entretienvehicule> findAll();

    List<Entretienvehicule> findRange(int[] range);

    int count();
    
}
