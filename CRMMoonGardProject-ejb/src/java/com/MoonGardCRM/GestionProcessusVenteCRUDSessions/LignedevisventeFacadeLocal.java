/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionProcessusVenteCRUDSessions;

import com.MoonGardCRM.GestionProcessusVenteEntities.Lignedevisvente;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Jerbi
 */
@Local
public interface LignedevisventeFacadeLocal {

    void create(Lignedevisvente lignedevisvente);

    Lignedevisvente edit(Lignedevisvente lignedevisvente);

    void remove(Lignedevisvente lignedevisvente);

    Lignedevisvente find(Object id);

    List<Lignedevisvente> findAll();

    List<Lignedevisvente> findRange(int[] range);

    int count();
    
}
