/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionProcessusVenteCRUDSessions;

import com.MoonGardCRM.GestionProcessusVenteEntities.Lignefactureavoirvente;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Jerbi
 */
@Local
public interface LignefactureavoirventeFacadeLocal {

    void create(Lignefactureavoirvente lignefactureavoirvente);

    Lignefactureavoirvente edit(Lignefactureavoirvente lignefactureavoirvente);

    void remove(Lignefactureavoirvente lignefactureavoirvente);

    Lignefactureavoirvente find(Object id);

    List<Lignefactureavoirvente> findAll();

    List<Lignefactureavoirvente> findRange(int[] range);

    int count();
    
}
