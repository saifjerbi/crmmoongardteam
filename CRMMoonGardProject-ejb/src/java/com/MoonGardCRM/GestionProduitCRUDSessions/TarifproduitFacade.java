/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionProduitCRUDSessions;

import com.MoonGardCRM.CRUDAbstractFacade.AbstractFacade;
import com.MoonGardCRM.GestionProduitEntities.Tarifproduit;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Jerbi
 */
@Stateless
public class TarifproduitFacade extends AbstractFacade<Tarifproduit> implements TarifproduitFacadeLocal {
    @PersistenceContext(unitName = "CRMMoonGardProject-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TarifproduitFacade() {
        super(Tarifproduit.class);
    }
    
}
