/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionUtilisateurEntities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jerbi
 */
@Entity
@Table(name = "caissier")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Caissier.findAll", query = "SELECT c FROM Caissier c"),
    @NamedQuery(name = "Caissier.findByIdUsr", query = "SELECT c FROM Caissier c WHERE c.idUsr = :idUsr"),
    @NamedQuery(name = "Caissier.findByNumcaisse", query = "SELECT c FROM Caissier c WHERE c.numcaisse = :numcaisse"),
    @NamedQuery(name = "Caissier.findByDateaffectationcaisse", query = "SELECT c FROM Caissier c WHERE c.dateaffectationcaisse = :dateaffectationcaisse")})
public class Caissier implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_usr")
    private Integer idUsr;
    @Column(name = "numcaisse")
    private Integer numcaisse;
    @Column(name = "dateaffectationcaisse")
    @Temporal(TemporalType.DATE)
    private Date dateaffectationcaisse;
    @JoinColumn(name = "id_usr", referencedColumnName = "id_usr", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Utilisateur utilisateur;

    public Caissier() {
    }

    public Caissier(Integer idUsr) {
        this.idUsr = idUsr;
    }

    public Integer getIdUsr() {
        return idUsr;
    }

    public void setIdUsr(Integer idUsr) {
        this.idUsr = idUsr;
    }

    public Integer getNumcaisse() {
        return numcaisse;
    }

    public void setNumcaisse(Integer numcaisse) {
        this.numcaisse = numcaisse;
    }

    public Date getDateaffectationcaisse() {
        return dateaffectationcaisse;
    }

    public void setDateaffectationcaisse(Date dateaffectationcaisse) {
        this.dateaffectationcaisse = dateaffectationcaisse;
    }

    public Utilisateur getUtilisateur() {
        return utilisateur;
    }

    public void setUtilisateur(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idUsr != null ? idUsr.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Caissier)) {
            return false;
        }
        Caissier other = (Caissier) object;
        if ((this.idUsr == null && other.idUsr != null) || (this.idUsr != null && !this.idUsr.equals(other.idUsr))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.MoonGardCRM.GestionUtilisateurEntities.Caissier[ idUsr=" + idUsr + " ]";
    }
    
}
