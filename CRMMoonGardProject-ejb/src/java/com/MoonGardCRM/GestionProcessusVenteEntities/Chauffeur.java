/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionProcessusVenteEntities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jerbi
 */
@Entity
@Table(name = "chauffeur")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Chauffeur.findAll", query = "SELECT c FROM Chauffeur c"),
    @NamedQuery(name = "Chauffeur.findByRefchaffeure", query = "SELECT c FROM Chauffeur c WHERE c.refchaffeure = :refchaffeure"),
    @NamedQuery(name = "Chauffeur.findByNom", query = "SELECT c FROM Chauffeur c WHERE c.nom = :nom"),
    @NamedQuery(name = "Chauffeur.findByPrenom", query = "SELECT c FROM Chauffeur c WHERE c.prenom = :prenom"),
    @NamedQuery(name = "Chauffeur.findByDateembauche", query = "SELECT c FROM Chauffeur c WHERE c.dateembauche = :dateembauche"),
    @NamedQuery(name = "Chauffeur.findByTypevehicule", query = "SELECT c FROM Chauffeur c WHERE c.typevehicule = :typevehicule")})
public class Chauffeur implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "refchaffeure")
    private Long refchaffeure;
    @Size(max = 254)
    @Column(name = "nom")
    private String nom;
    @Size(max = 254)
    @Column(name = "prenom")
    private String prenom;
    @Column(name = "dateembauche")
    @Temporal(TemporalType.DATE)
    private Date dateembauche;
    @Size(max = 254)
    @Column(name = "typevehicule")
    private String typevehicule;
    @OneToMany(mappedBy = "refchaffeure")
    private Collection<Vehiculelivraison> vehiculelivraisonCollection;

    public Chauffeur() {
    }

    public Chauffeur(Long refchaffeure) {
        this.refchaffeure = refchaffeure;
    }

    public Long getRefchaffeure() {
        return refchaffeure;
    }

    public void setRefchaffeure(Long refchaffeure) {
        this.refchaffeure = refchaffeure;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Date getDateembauche() {
        return dateembauche;
    }

    public void setDateembauche(Date dateembauche) {
        this.dateembauche = dateembauche;
    }

    public String getTypevehicule() {
        return typevehicule;
    }

    public void setTypevehicule(String typevehicule) {
        this.typevehicule = typevehicule;
    }

    @XmlTransient
    public Collection<Vehiculelivraison> getVehiculelivraisonCollection() {
        return vehiculelivraisonCollection;
    }

    public void setVehiculelivraisonCollection(Collection<Vehiculelivraison> vehiculelivraisonCollection) {
        this.vehiculelivraisonCollection = vehiculelivraisonCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refchaffeure != null ? refchaffeure.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Chauffeur)) {
            return false;
        }
        Chauffeur other = (Chauffeur) object;
        if ((this.refchaffeure == null && other.refchaffeure != null) || (this.refchaffeure != null && !this.refchaffeure.equals(other.refchaffeure))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.MoonGardCRM.GestionProcessusVenteEntities.Chauffeur[ refchaffeure=" + refchaffeure + " ]";
    }
    
}
