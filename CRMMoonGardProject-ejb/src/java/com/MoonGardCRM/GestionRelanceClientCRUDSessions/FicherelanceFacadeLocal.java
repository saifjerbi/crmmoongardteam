/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionRelanceClientCRUDSessions;

import com.MoonGardCRM.GestionRelanceClientEntities.Ficherelance;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Jerbi
 */
@Local
public interface FicherelanceFacadeLocal {

    void create(Ficherelance ficherelance);

    Ficherelance edit(Ficherelance ficherelance);

    void remove(Ficherelance ficherelance);

    Ficherelance find(Object id);

    List<Ficherelance> findAll();

    List<Ficherelance> findRange(int[] range);

    int count();
    
}
