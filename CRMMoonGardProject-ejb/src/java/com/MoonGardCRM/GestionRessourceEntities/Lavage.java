/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionRessourceEntities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jerbi
 */
@Entity
@Table(name = "lavage")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Lavage.findAll", query = "SELECT l FROM Lavage l"),
    @NamedQuery(name = "Lavage.findByRefentretienvehicule", query = "SELECT l FROM Lavage l WHERE l.refentretienvehicule = :refentretienvehicule"),
    @NamedQuery(name = "Lavage.findByLieu", query = "SELECT l FROM Lavage l WHERE l.lieu = :lieu")})
public class Lavage implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "refentretienvehicule")
    private Long refentretienvehicule;
    @Size(max = 254)
    @Column(name = "lieu")
    private String lieu;
    @JoinColumn(name = "refentretienvehicule", referencedColumnName = "refentretienvehicule", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Entretienvehicule entretienvehicule;

    public Lavage() {
    }

    public Lavage(Long refentretienvehicule) {
        this.refentretienvehicule = refentretienvehicule;
    }

    public Long getRefentretienvehicule() {
        return refentretienvehicule;
    }

    public void setRefentretienvehicule(Long refentretienvehicule) {
        this.refentretienvehicule = refentretienvehicule;
    }

    public String getLieu() {
        return lieu;
    }

    public void setLieu(String lieu) {
        this.lieu = lieu;
    }

    public Entretienvehicule getEntretienvehicule() {
        return entretienvehicule;
    }

    public void setEntretienvehicule(Entretienvehicule entretienvehicule) {
        this.entretienvehicule = entretienvehicule;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refentretienvehicule != null ? refentretienvehicule.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Lavage)) {
            return false;
        }
        Lavage other = (Lavage) object;
        if ((this.refentretienvehicule == null && other.refentretienvehicule != null) || (this.refentretienvehicule != null && !this.refentretienvehicule.equals(other.refentretienvehicule))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.MoonGardCRM.GestionRessourceEntities.Lavage[ refentretienvehicule=" + refentretienvehicule + " ]";
    }
    
}
