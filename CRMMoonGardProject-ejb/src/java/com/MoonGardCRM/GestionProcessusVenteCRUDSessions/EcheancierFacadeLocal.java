/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionProcessusVenteCRUDSessions;

import com.MoonGardCRM.GestionProcessusVenteEntities.Echeancier;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Jerbi
 */
@Local
public interface EcheancierFacadeLocal {

    void create(Echeancier echeancier);

    Echeancier edit(Echeancier echeancier);

    void remove(Echeancier echeancier);

    Echeancier find(Object id);

    List<Echeancier> findAll();

    List<Echeancier> findRange(int[] range);

    int count();
    
}
