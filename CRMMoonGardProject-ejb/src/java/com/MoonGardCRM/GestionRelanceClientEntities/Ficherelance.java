/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionRelanceClientEntities;

import com.MoonGardCRM.GestionContactEntities.Contact;
import com.MoonGardCRM.GestionProcessusVenteEntities.Echeancier;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jerbi
 */
@Entity
@Table(name = "ficherelance")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Ficherelance.findAll", query = "SELECT f FROM Ficherelance f"),
    @NamedQuery(name = "Ficherelance.findByRefficherelance", query = "SELECT f FROM Ficherelance f WHERE f.refficherelance = :refficherelance"),
    @NamedQuery(name = "Ficherelance.findByNomderelance", query = "SELECT f FROM Ficherelance f WHERE f.nomderelance = :nomderelance"),
    @NamedQuery(name = "Ficherelance.findByNombrejourlimitepay", query = "SELECT f FROM Ficherelance f WHERE f.nombrejourlimitepay = :nombrejourlimitepay"),
    @NamedQuery(name = "Ficherelance.findByObjetrelance", query = "SELECT f FROM Ficherelance f WHERE f.objetrelance = :objetrelance"),
    @NamedQuery(name = "Ficherelance.findByMessage", query = "SELECT f FROM Ficherelance f WHERE f.message = :message")})
public class Ficherelance implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "refficherelance")
    private Long refficherelance;
    @Size(max = 254)
    @Column(name = "nomderelance")
    private String nomderelance;
    @Column(name = "nombrejourlimitepay")
    private Integer nombrejourlimitepay;
    @Size(max = 254)
    @Column(name = "objetrelance")
    private String objetrelance;
    @Size(max = 254)
    @Column(name = "message")
    private String message;
    @JoinColumn(name = "refrelanceclient", referencedColumnName = "refrelanceclient")
    @ManyToOne
    private Relanceclient refrelanceclient;
    @JoinColumn(name = "refecheancier", referencedColumnName = "refecheancier")
    @ManyToOne
    private Echeancier refecheancier;
    @JoinColumn(name = "idcontact", referencedColumnName = "idcontact")
    @ManyToOne
    private Contact idcontact;

    public Ficherelance() {
    }

    public Ficherelance(Long refficherelance) {
        this.refficherelance = refficherelance;
    }

    public Long getRefficherelance() {
        return refficherelance;
    }

    public void setRefficherelance(Long refficherelance) {
        this.refficherelance = refficherelance;
    }

    public String getNomderelance() {
        return nomderelance;
    }

    public void setNomderelance(String nomderelance) {
        this.nomderelance = nomderelance;
    }

    public Integer getNombrejourlimitepay() {
        return nombrejourlimitepay;
    }

    public void setNombrejourlimitepay(Integer nombrejourlimitepay) {
        this.nombrejourlimitepay = nombrejourlimitepay;
    }

    public String getObjetrelance() {
        return objetrelance;
    }

    public void setObjetrelance(String objetrelance) {
        this.objetrelance = objetrelance;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Relanceclient getRefrelanceclient() {
        return refrelanceclient;
    }

    public void setRefrelanceclient(Relanceclient refrelanceclient) {
        this.refrelanceclient = refrelanceclient;
    }

    public Echeancier getRefecheancier() {
        return refecheancier;
    }

    public void setRefecheancier(Echeancier refecheancier) {
        this.refecheancier = refecheancier;
    }

    public Contact getIdcontact() {
        return idcontact;
    }

    public void setIdcontact(Contact idcontact) {
        this.idcontact = idcontact;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refficherelance != null ? refficherelance.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ficherelance)) {
            return false;
        }
        Ficherelance other = (Ficherelance) object;
        if ((this.refficherelance == null && other.refficherelance != null) || (this.refficherelance != null && !this.refficherelance.equals(other.refficherelance))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.MoonGardCRM.GestionRelanceClientEntities.Ficherelance[ refficherelance=" + refficherelance + " ]";
    }
    
}
