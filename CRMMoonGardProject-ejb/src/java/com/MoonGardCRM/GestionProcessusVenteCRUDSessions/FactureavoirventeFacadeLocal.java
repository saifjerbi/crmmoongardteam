/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionProcessusVenteCRUDSessions;

import com.MoonGardCRM.GestionProcessusVenteEntities.Factureavoirvente;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Jerbi
 */
@Local
public interface FactureavoirventeFacadeLocal {

    void create(Factureavoirvente factureavoirvente);

    Factureavoirvente edit(Factureavoirvente factureavoirvente);

    void remove(Factureavoirvente factureavoirvente);

    Factureavoirvente find(Object id);

    List<Factureavoirvente> findAll();

    List<Factureavoirvente> findRange(int[] range);

    int count();
    
}
