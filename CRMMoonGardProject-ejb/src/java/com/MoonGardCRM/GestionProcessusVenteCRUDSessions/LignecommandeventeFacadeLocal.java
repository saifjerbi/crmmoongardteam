/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionProcessusVenteCRUDSessions;

import com.MoonGardCRM.GestionProcessusVenteEntities.Lignecommandevente;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Jerbi
 */
@Local
public interface LignecommandeventeFacadeLocal {

    void create(Lignecommandevente lignecommandevente);

    Lignecommandevente edit(Lignecommandevente lignecommandevente);

    void remove(Lignecommandevente lignecommandevente);

    Lignecommandevente find(Object id);

    List<Lignecommandevente> findAll();

    List<Lignecommandevente> findRange(int[] range);

    int count();
    
}
