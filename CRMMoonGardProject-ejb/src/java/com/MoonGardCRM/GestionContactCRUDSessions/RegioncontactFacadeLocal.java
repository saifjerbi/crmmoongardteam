/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionContactCRUDSessions;

import com.MoonGardCRM.GestionContactEntities.Regioncontact;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Jerbi
 */
@Local
public interface RegioncontactFacadeLocal {

    void create(Regioncontact regioncontact);

    Regioncontact edit(Regioncontact regioncontact);

    void remove(Regioncontact regioncontact);

    Regioncontact find(Object id);

    List<Regioncontact> findAll();

    List<Regioncontact> findRange(int[] range);

    int count();
    
}
