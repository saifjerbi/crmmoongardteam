/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionContactEntities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Jerbi
 */
@Embeddable
public class AdressecontactPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "idcontact")
    private long idcontact;
    @Basic(optional = false)
    @NotNull
    @Column(name = "refadressecontact")
    private long refadressecontact;

    public AdressecontactPK() {
    }

    public AdressecontactPK(long idcontact, long refadressecontact) {
        this.idcontact = idcontact;
        this.refadressecontact = refadressecontact;
    }

    public long getIdcontact() {
        return idcontact;
    }

    public void setIdcontact(long idcontact) {
        this.idcontact = idcontact;
    }

    public long getRefadressecontact() {
        return refadressecontact;
    }

    public void setRefadressecontact(long refadressecontact) {
        this.refadressecontact = refadressecontact;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idcontact;
        hash += (int) refadressecontact;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AdressecontactPK)) {
            return false;
        }
        AdressecontactPK other = (AdressecontactPK) object;
        if (this.idcontact != other.idcontact) {
            return false;
        }
        if (this.refadressecontact != other.refadressecontact) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.MoonGardCRM.GestionContactEntities.AdressecontactPK[ idcontact=" + idcontact + ", refadressecontact=" + refadressecontact + " ]";
    }
    
}
