/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionProcessusVenteCRUDSessions;

import com.MoonGardCRM.GestionProcessusVenteEntities.Payementfacturevente;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Jerbi
 */
@Local
public interface PayementfactureventeFacadeLocal {

    void create(Payementfacturevente payementfacturevente);

    Payementfacturevente edit(Payementfacturevente payementfacturevente);

    void remove(Payementfacturevente payementfacturevente);

    Payementfacturevente find(Object id);

    List<Payementfacturevente> findAll();

    List<Payementfacturevente> findRange(int[] range);

    int count();
    
}
