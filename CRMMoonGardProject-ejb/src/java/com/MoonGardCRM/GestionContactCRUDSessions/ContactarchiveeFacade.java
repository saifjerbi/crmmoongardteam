/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionContactCRUDSessions;

import com.MoonGardCRM.CRUDAbstractFacade.AbstractFacade;
import com.MoonGardCRM.GestionContactEntities.Contactarchivee;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Jerbi
 */
@Stateless
public class ContactarchiveeFacade extends AbstractFacade<Contactarchivee> implements ContactarchiveeFacadeLocal {
    @PersistenceContext(unitName = "CRMMoonGardProject-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ContactarchiveeFacade() {
        super(Contactarchivee.class);
    }
    
}
