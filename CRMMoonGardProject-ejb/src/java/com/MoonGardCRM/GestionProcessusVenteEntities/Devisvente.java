/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionProcessusVenteEntities;

import com.MoonGardCRM.GestionContactEntities.Client;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jerbi
 */
@Entity
@Table(name = "devisvente")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Devisvente.findAll", query = "SELECT d FROM Devisvente d"),
    @NamedQuery(name = "Devisvente.findByNumerodev", query = "SELECT d FROM Devisvente d WHERE d.numerodev = :numerodev"),
    @NamedQuery(name = "Devisvente.findByDatecreation", query = "SELECT d FROM Devisvente d WHERE d.datecreation = :datecreation"),
    @NamedQuery(name = "Devisvente.findByLimitevalidite", query = "SELECT d FROM Devisvente d WHERE d.limitevalidite = :limitevalidite"),
    @NamedQuery(name = "Devisvente.findByTotalttc", query = "SELECT d FROM Devisvente d WHERE d.totalttc = :totalttc"),
    @NamedQuery(name = "Devisvente.findByTotalht", query = "SELECT d FROM Devisvente d WHERE d.totalht = :totalht"),
    @NamedQuery(name = "Devisvente.findByTotaltva", query = "SELECT d FROM Devisvente d WHERE d.totaltva = :totaltva"),
    @NamedQuery(name = "Devisvente.findByNetapayer", query = "SELECT d FROM Devisvente d WHERE d.netapayer = :netapayer"),
    @NamedQuery(name = "Devisvente.findByMessageentete", query = "SELECT d FROM Devisvente d WHERE d.messageentete = :messageentete"),
    @NamedQuery(name = "Devisvente.findByNotebaspage", query = "SELECT d FROM Devisvente d WHERE d.notebaspage = :notebaspage"),
    @NamedQuery(name = "Devisvente.findByModepayement", query = "SELECT d FROM Devisvente d WHERE d.modepayement = :modepayement"),
    @NamedQuery(name = "Devisvente.findByEtat", query = "SELECT d FROM Devisvente d WHERE d.etat = :etat")})
public class Devisvente implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "numerodev")
    private Long numerodev;
    @Column(name = "datecreation")
    @Temporal(TemporalType.DATE)
    private Date datecreation;
    @Column(name = "limitevalidite")
    private Integer limitevalidite;
    @Column(name = "totalttc")
    private Integer totalttc;
    @Column(name = "totalht")
    private Integer totalht;
    @Column(name = "totaltva")
    private Integer totaltva;
    @Column(name = "netapayer")
    private Integer netapayer;
    @Size(max = 254)
    @Column(name = "messageentete")
    private String messageentete;
    @Size(max = 254)
    @Column(name = "notebaspage")
    private String notebaspage;
    @Size(max = 254)
    @Column(name = "modepayement")
    private String modepayement;
    @Size(max = 254)
    @Column(name = "etat")
    private String etat;
    @JoinColumn(name = "refcommercialvente", referencedColumnName = "refcommercialvente")
    @ManyToOne
    private Commercialvente refcommercialvente;
    @JoinColumn(name = "numerocom", referencedColumnName = "numerocom")
    @ManyToOne
    private Commandevente numerocom;
    @JoinColumn(name = "idcontact", referencedColumnName = "idcontact")
    @ManyToOne
    private Client idcontact;
    @OneToMany(mappedBy = "numerodev")
    private Collection<Lignedevisvente> lignedevisventeCollection;

    public Devisvente() {
    }

    public Devisvente(Long numerodev) {
        this.numerodev = numerodev;
    }

    public Long getNumerodev() {
        return numerodev;
    }

    public void setNumerodev(Long numerodev) {
        this.numerodev = numerodev;
    }

    public Date getDatecreation() {
        return datecreation;
    }

    public void setDatecreation(Date datecreation) {
        this.datecreation = datecreation;
    }

    public Integer getLimitevalidite() {
        return limitevalidite;
    }

    public void setLimitevalidite(Integer limitevalidite) {
        this.limitevalidite = limitevalidite;
    }

    public Integer getTotalttc() {
        return totalttc;
    }

    public void setTotalttc(Integer totalttc) {
        this.totalttc = totalttc;
    }

    public Integer getTotalht() {
        return totalht;
    }

    public void setTotalht(Integer totalht) {
        this.totalht = totalht;
    }

    public Integer getTotaltva() {
        return totaltva;
    }

    public void setTotaltva(Integer totaltva) {
        this.totaltva = totaltva;
    }

    public Integer getNetapayer() {
        return netapayer;
    }

    public void setNetapayer(Integer netapayer) {
        this.netapayer = netapayer;
    }

    public String getMessageentete() {
        return messageentete;
    }

    public void setMessageentete(String messageentete) {
        this.messageentete = messageentete;
    }

    public String getNotebaspage() {
        return notebaspage;
    }

    public void setNotebaspage(String notebaspage) {
        this.notebaspage = notebaspage;
    }

    public String getModepayement() {
        return modepayement;
    }

    public void setModepayement(String modepayement) {
        this.modepayement = modepayement;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public Commercialvente getRefcommercialvente() {
        return refcommercialvente;
    }

    public void setRefcommercialvente(Commercialvente refcommercialvente) {
        this.refcommercialvente = refcommercialvente;
    }

    public Commandevente getNumerocom() {
        return numerocom;
    }

    public void setNumerocom(Commandevente numerocom) {
        this.numerocom = numerocom;
    }

    public Client getIdcontact() {
        return idcontact;
    }

    public void setIdcontact(Client idcontact) {
        this.idcontact = idcontact;
    }

    @XmlTransient
    public Collection<Lignedevisvente> getLignedevisventeCollection() {
        return lignedevisventeCollection;
    }

    public void setLignedevisventeCollection(Collection<Lignedevisvente> lignedevisventeCollection) {
        this.lignedevisventeCollection = lignedevisventeCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (numerodev != null ? numerodev.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Devisvente)) {
            return false;
        }
        Devisvente other = (Devisvente) object;
        if ((this.numerodev == null && other.numerodev != null) || (this.numerodev != null && !this.numerodev.equals(other.numerodev))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.MoonGardCRM.GestionProcessusVenteEntities.Devisvente[ numerodev=" + numerodev + " ]";
    }
    
}
