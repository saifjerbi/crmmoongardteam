/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionProcessusVenteCRUDSessions;

import com.MoonGardCRM.GestionProcessusVenteEntities.Livraisonvente;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Jerbi
 */
@Local
public interface LivraisonventeFacadeLocal {

    void create(Livraisonvente livraisonvente);

    Livraisonvente edit(Livraisonvente livraisonvente);

    void remove(Livraisonvente livraisonvente);

    Livraisonvente find(Object id);

    List<Livraisonvente> findAll();

    List<Livraisonvente> findRange(int[] range);

    int count();
    
}
