/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionProduitEntities;

import com.MoonGardCRM.GestionMarketingEntities.Newslettercontact;
import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jerbi
 */
@Entity
@Table(name = "newsletter")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Newsletter.findAll", query = "SELECT n FROM Newsletter n"),
    @NamedQuery(name = "Newsletter.findByRefnewsletter", query = "SELECT n FROM Newsletter n WHERE n.refnewsletter = :refnewsletter"),
    @NamedQuery(name = "Newsletter.findByNom", query = "SELECT n FROM Newsletter n WHERE n.nom = :nom"),
    @NamedQuery(name = "Newsletter.findByPrenom", query = "SELECT n FROM Newsletter n WHERE n.prenom = :prenom"),
    @NamedQuery(name = "Newsletter.findByType", query = "SELECT n FROM Newsletter n WHERE n.type = :type"),
    @NamedQuery(name = "Newsletter.findByContenu", query = "SELECT n FROM Newsletter n WHERE n.contenu = :contenu")})
public class Newsletter implements Serializable {
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "newsletter")
    private Collection<Newslettercontact> newslettercontactCollection;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "refnewsletter")
    private Long refnewsletter;
    @Size(max = 254)
    @Column(name = "nom")
    private String nom;
    @Size(max = 254)
    @Column(name = "prenom")
    private String prenom;
    @Size(max = 254)
    @Column(name = "type")
    private String type;
    @Size(max = 254)
    @Column(name = "contenu")
    private String contenu;
    @ManyToMany(mappedBy = "newsletterCollection")
    private Collection<Catalogueproduit> catalogueproduitCollection;

    public Newsletter() {
    }

    public Newsletter(Long refnewsletter) {
        this.refnewsletter = refnewsletter;
    }

    public Long getRefnewsletter() {
        return refnewsletter;
    }

    public void setRefnewsletter(Long refnewsletter) {
        this.refnewsletter = refnewsletter;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getContenu() {
        return contenu;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    @XmlTransient
    public Collection<Catalogueproduit> getCatalogueproduitCollection() {
        return catalogueproduitCollection;
    }

    public void setCatalogueproduitCollection(Collection<Catalogueproduit> catalogueproduitCollection) {
        this.catalogueproduitCollection = catalogueproduitCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refnewsletter != null ? refnewsletter.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Newsletter)) {
            return false;
        }
        Newsletter other = (Newsletter) object;
        if ((this.refnewsletter == null && other.refnewsletter != null) || (this.refnewsletter != null && !this.refnewsletter.equals(other.refnewsletter))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.MoonGardCRM.GestionProduitEntities.Newsletter[ refnewsletter=" + refnewsletter + " ]";
    }

    @XmlTransient
    public Collection<Newslettercontact> getNewslettercontactCollection() {
        return newslettercontactCollection;
    }

    public void setNewslettercontactCollection(Collection<Newslettercontact> newslettercontactCollection) {
        this.newslettercontactCollection = newslettercontactCollection;
    }
    
}
