/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionProcessusVenteEntities;

import com.MoonGardCRM.GestionContactEntities.Client;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jerbi
 */
@Entity
@Table(name = "recouvrementclient")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Recouvrementclient.findAll", query = "SELECT r FROM Recouvrementclient r"),
    @NamedQuery(name = "Recouvrementclient.findByRefrecouvrementclient", query = "SELECT r FROM Recouvrementclient r WHERE r.refrecouvrementclient = :refrecouvrementclient"),
    @NamedQuery(name = "Recouvrementclient.findByDatecreation", query = "SELECT r FROM Recouvrementclient r WHERE r.datecreation = :datecreation"),
    @NamedQuery(name = "Recouvrementclient.findByDatemodification", query = "SELECT r FROM Recouvrementclient r WHERE r.datemodification = :datemodification"),
    @NamedQuery(name = "Recouvrementclient.findByDatefin", query = "SELECT r FROM Recouvrementclient r WHERE r.datefin = :datefin"),
    @NamedQuery(name = "Recouvrementclient.findByNombredemois", query = "SELECT r FROM Recouvrementclient r WHERE r.nombredemois = :nombredemois"),
    @NamedQuery(name = "Recouvrementclient.findByTotal", query = "SELECT r FROM Recouvrementclient r WHERE r.total = :total"),
    @NamedQuery(name = "Recouvrementclient.findByDateprochainecheancier", query = "SELECT r FROM Recouvrementclient r WHERE r.dateprochainecheancier = :dateprochainecheancier"),
    @NamedQuery(name = "Recouvrementclient.findByDatedernierecheancier", query = "SELECT r FROM Recouvrementclient r WHERE r.datedernierecheancier = :datedernierecheancier"),
    @NamedQuery(name = "Recouvrementclient.findByTotalpayer", query = "SELECT r FROM Recouvrementclient r WHERE r.totalpayer = :totalpayer"),
    @NamedQuery(name = "Recouvrementclient.findByResteapayer", query = "SELECT r FROM Recouvrementclient r WHERE r.resteapayer = :resteapayer"),
    @NamedQuery(name = "Recouvrementclient.findByDateannulation", query = "SELECT r FROM Recouvrementclient r WHERE r.dateannulation = :dateannulation")})
public class Recouvrementclient implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "refrecouvrementclient")
    private Long refrecouvrementclient;
    @Column(name = "datecreation")
    @Temporal(TemporalType.DATE)
    private Date datecreation;
    @Column(name = "datemodification")
    @Temporal(TemporalType.DATE)
    private Date datemodification;
    @Column(name = "datefin")
    @Temporal(TemporalType.DATE)
    private Date datefin;
    @Column(name = "nombredemois")
    private Integer nombredemois;
    @Column(name = "total")
    private Integer total;
    @Column(name = "dateprochainecheancier")
    @Temporal(TemporalType.DATE)
    private Date dateprochainecheancier;
    @Column(name = "datedernierecheancier")
    @Temporal(TemporalType.DATE)
    private Date datedernierecheancier;
    @Column(name = "totalpayer")
    private BigInteger totalpayer;
    @Column(name = "resteapayer")
    private BigInteger resteapayer;
    @Column(name = "dateannulation")
    @Temporal(TemporalType.DATE)
    private Date dateannulation;
    @OneToMany(mappedBy = "refrecouvrementclient")
    private Collection<Echeancier> echeancierCollection;
    @JoinColumn(name = "refcompteclient", referencedColumnName = "refcompteclient")
    @ManyToOne
    private Compteclient refcompteclient;
    @JoinColumn(name = "idcontact", referencedColumnName = "idcontact")
    @ManyToOne
    private Client idcontact;

    public Recouvrementclient() {
    }

    public Recouvrementclient(Long refrecouvrementclient) {
        this.refrecouvrementclient = refrecouvrementclient;
    }

    public Long getRefrecouvrementclient() {
        return refrecouvrementclient;
    }

    public void setRefrecouvrementclient(Long refrecouvrementclient) {
        this.refrecouvrementclient = refrecouvrementclient;
    }

    public Date getDatecreation() {
        return datecreation;
    }

    public void setDatecreation(Date datecreation) {
        this.datecreation = datecreation;
    }

    public Date getDatemodification() {
        return datemodification;
    }

    public void setDatemodification(Date datemodification) {
        this.datemodification = datemodification;
    }

    public Date getDatefin() {
        return datefin;
    }

    public void setDatefin(Date datefin) {
        this.datefin = datefin;
    }

    public Integer getNombredemois() {
        return nombredemois;
    }

    public void setNombredemois(Integer nombredemois) {
        this.nombredemois = nombredemois;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Date getDateprochainecheancier() {
        return dateprochainecheancier;
    }

    public void setDateprochainecheancier(Date dateprochainecheancier) {
        this.dateprochainecheancier = dateprochainecheancier;
    }

    public Date getDatedernierecheancier() {
        return datedernierecheancier;
    }

    public void setDatedernierecheancier(Date datedernierecheancier) {
        this.datedernierecheancier = datedernierecheancier;
    }

    public BigInteger getTotalpayer() {
        return totalpayer;
    }

    public void setTotalpayer(BigInteger totalpayer) {
        this.totalpayer = totalpayer;
    }

    public BigInteger getResteapayer() {
        return resteapayer;
    }

    public void setResteapayer(BigInteger resteapayer) {
        this.resteapayer = resteapayer;
    }

    public Date getDateannulation() {
        return dateannulation;
    }

    public void setDateannulation(Date dateannulation) {
        this.dateannulation = dateannulation;
    }

    @XmlTransient
    public Collection<Echeancier> getEcheancierCollection() {
        return echeancierCollection;
    }

    public void setEcheancierCollection(Collection<Echeancier> echeancierCollection) {
        this.echeancierCollection = echeancierCollection;
    }

    public Compteclient getRefcompteclient() {
        return refcompteclient;
    }

    public void setRefcompteclient(Compteclient refcompteclient) {
        this.refcompteclient = refcompteclient;
    }

    public Client getIdcontact() {
        return idcontact;
    }

    public void setIdcontact(Client idcontact) {
        this.idcontact = idcontact;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refrecouvrementclient != null ? refrecouvrementclient.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Recouvrementclient)) {
            return false;
        }
        Recouvrementclient other = (Recouvrementclient) object;
        if ((this.refrecouvrementclient == null && other.refrecouvrementclient != null) || (this.refrecouvrementclient != null && !this.refrecouvrementclient.equals(other.refrecouvrementclient))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.MoonGardCRM.GestionProcessusVenteEntities.Recouvrementclient[ refrecouvrementclient=" + refrecouvrementclient + " ]";
    }
    
}
