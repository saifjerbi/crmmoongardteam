/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MoonGardCRM.GestionProduitEntities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jerbi
 */
@Entity
@Table(name = "familleproduit")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Familleproduit.findAll", query = "SELECT f FROM Familleproduit f"),
    @NamedQuery(name = "Familleproduit.findByReffamilleproduit", query = "SELECT f FROM Familleproduit f WHERE f.reffamilleproduit = :reffamilleproduit"),
    @NamedQuery(name = "Familleproduit.findByNomfamille", query = "SELECT f FROM Familleproduit f WHERE f.nomfamille = :nomfamille"),
    @NamedQuery(name = "Familleproduit.findByDatecreation", query = "SELECT f FROM Familleproduit f WHERE f.datecreation = :datecreation"),
    @NamedQuery(name = "Familleproduit.findByDatemodification", query = "SELECT f FROM Familleproduit f WHERE f.datemodification = :datemodification"),
    @NamedQuery(name = "Familleproduit.findByEtat", query = "SELECT f FROM Familleproduit f WHERE f.etat = :etat")})
public class Familleproduit implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "reffamilleproduit")
    private Long reffamilleproduit;
    @Size(max = 254)
    @Column(name = "nomfamille")
    private String nomfamille;
    @Column(name = "datecreation")
    @Temporal(TemporalType.DATE)
    private Date datecreation;
    @Column(name = "datemodification")
    @Temporal(TemporalType.DATE)
    private Date datemodification;
    @Size(max = 254)
    @Column(name = "etat")
    private String etat;
    @OneToMany(mappedBy = "reffamilleproduit")
    private Collection<Produit> produitCollection;

    public Familleproduit() {
    }

    public Familleproduit(Long reffamilleproduit) {
        this.reffamilleproduit = reffamilleproduit;
    }

    public Long getReffamilleproduit() {
        return reffamilleproduit;
    }

    public void setReffamilleproduit(Long reffamilleproduit) {
        this.reffamilleproduit = reffamilleproduit;
    }

    public String getNomfamille() {
        return nomfamille;
    }

    public void setNomfamille(String nomfamille) {
        this.nomfamille = nomfamille;
    }

    public Date getDatecreation() {
        return datecreation;
    }

    public void setDatecreation(Date datecreation) {
        this.datecreation = datecreation;
    }

    public Date getDatemodification() {
        return datemodification;
    }

    public void setDatemodification(Date datemodification) {
        this.datemodification = datemodification;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    @XmlTransient
    public Collection<Produit> getProduitCollection() {
        return produitCollection;
    }

    public void setProduitCollection(Collection<Produit> produitCollection) {
        this.produitCollection = produitCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (reffamilleproduit != null ? reffamilleproduit.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Familleproduit)) {
            return false;
        }
        Familleproduit other = (Familleproduit) object;
        if ((this.reffamilleproduit == null && other.reffamilleproduit != null) || (this.reffamilleproduit != null && !this.reffamilleproduit.equals(other.reffamilleproduit))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.MoonGardCRM.GestionProduitEntities.Familleproduit[ reffamilleproduit=" + reffamilleproduit + " ]";
    }
    
}
